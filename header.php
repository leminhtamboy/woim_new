<?php 
include_once("super_process_Menu.php");
$process_menu= new Super_Process_Menu("category","id");
$img_logo=Super_Core::$url_web."images/logo_woim.png";
?>
<header>
	<script type="text/javascript">
		function replaceSearch(elem){
			jQuery(".search > ul > li").attr("class","");
			jQuery(elem).attr("class","active");
			var condition=jQuery(elem).attr("data-search");
			jQuery("#query-condition").val(condition);
		}
		function submitForm(){
			var $querySearch=jQuery("#query_search").val();
			var $conditionSearch=jQuery("#query-condition").val();
			$querySearch=$querySearch.replace("'","");
			$conditionSearch=$conditionSearch.replace("'","");
			var mainURL='<?php echo $superCore->getAjaxUrl() ?>';
			var url=mainURL+""+"Ajax_Save_Search.php";
			var user_id='<?php echo $user_id ?>';
			jQuery.ajax({
				type:'post',
				url:url,
				data:{query_search:$querySearch,conditionSearch:$conditionSearch,user_id:user_id},
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				cache: false,
				asyncBoolean:false,
				complete:function(){},
				error:function(err){

				},
				success:function(result){
					jQuery("#form_search").attr('action', '/search-' + $querySearch + '-' + $conditionSearch + '.html');
					jQuery("#form_search").submit();
				}
			});

		}
		function load_recent_search(elem){
			var textSearch=jQuery(elem).val();
			var condition=jQuery("#query-condition").val();
			var mainURL='<?php echo $superCore->getAjaxUrl() ?>';
			var url=mainURL+""+"Ajax_Load_Search.php";
			jQuery.ajax({
				type:'post',
				url:url,
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				data:{user_id:'<?php echo $user_id; ?>'},
				cache: false,
				asyncBoolean:false,
				complete:function(){},
				error:function(err){

				},
				success:function(result){
					jQuery("#suggested").html(result);
				}
			});
		}
		function find_album_tuong_doi(elem){
			var textSearch=jQuery(elem).val();
			var condition=jQuery("#query-condition").val();
			var mainURL='<?php echo $superCore->getAjaxUrl() ?>';
			var url=mainURL+""+"Ajax_Search.php";
			jQuery.ajax({
				type:'post',
				url:url,
				data:{input_search:textSearch,condition:condition},
				contentType:'application/x-www-form-urlencoded; charset=UTF-8',
				cache: false,
				asyncBoolean:false,
				complete:function(){},
				error:function(err){

				},
				success:function(result){
					jQuery("#suggested").html(result);
				}
			});
		}
		<?php if(isset($_REQUEST["c"])){ ?>
		jQuery(document).ready(function(){
			var isRequet='<?php echo $_REQUEST["c"] ?>';
			if(isRequet=="song"){
				jQuery(".search > ul > li").attr("class","");
				var elemSong=jQuery("#li_song");
				elemSong.attr("class","active");
				var condition=elemSong.attr("data-search");
				jQuery("#query-condition").val(condition);
				jQuery("#query_search").val('<?php echo $_REQUEST["q"] ?>');
			}
			if(isRequet=="album"){
				jQuery(".search > ul > li").attr("class","");
				var elemSong=jQuery("#li_album");
				elemSong.attr("class","active");
				var condition=elemSong.attr("data-search");
				jQuery("#query-condition").val(condition);
				jQuery("#query_search").val('<?php echo $_REQUEST["q"] ?>');
			}
			if(isRequet=="nghesi"){
				jQuery(".search > ul > li").attr("class","");
				var elemSong=jQuery("#li_nghe_si");
				elemSong.attr("class","active");
				var condition=elemSong.attr("data-search");
				jQuery("#query-condition").val(condition);
				jQuery("#query_search").val('<?php echo $_REQUEST["q"] ?>');
			}
		});
		<?php } ?>
	</script>
	<div id="header">
		<div class="logo">
			<a href="<?php echo Super_Core::$url_web; ?>"><img src="<?php echo $img_logo ?>" alt="Woim.me,nhạc không lời,nhac khong lời,radio show" title="Woim.me,nhạc không lời,nhac khong lời,radio show"/></a>
			<div class="clearfix">
				<button id="menu_button_tabblet"></button>
				<button id="login_button_tabblet"></button>
				<button id="chat_button"></button>
				<button id="online_button"></button>
			</div>
		</div>

		<div class="search">
			<button id="close_search"></button>
			<ul>
				<li id="li_album" class="active" data-search="album" onclick="replaceSearch(this)">
					<a href="javascript:void(0)"  class="a-href-search" onclick="replaceSearch(this)">Album</a>
				</li>
				<li id="li_song" onclick="replaceSearch(this)" data-search="song">
					<a href="javascript:void(0)"  class="a-href-search" onclick="replaceSearch(this)">Song</a>
				</li>
				<li id="li_nghe_si" data-search="nghesi"  onclick="replaceSearch(this)">
					<a href="javascript:void(0)"  class="a-href-search" onclick="replaceSearch(this)">Nghệ Sĩ</a>
				</li>
				<li onclick="replaceSearch(this)" data-search="nang-cao">
					<a href="/tim-kiem-nang-cao.html" class="a-href-search" onclick="replaceSearch(this)">Nâng Cao</a>
				</li>
			</ul>
			<div class="input-search" align="right">
				<form action="" id="form_search" name="form_search" method="post">
					<input type="text" class="query-search" onkeyup="find_album_tuong_doi(this)" onclick="load_recent_search()" id="query_search" value="" name="query_search"/>
					<div class="suggest-search" id="suggested">

					</div>
					<input type="hidden" id="query-condition" class="query-search" value="album" name="query_condition"/>
					<input type="submit" class="search-button" id="tim_kiem" name="tim_kiem" onclick="submitForm();" value="Tìm kiếm"/>
				</form>
			</div>
		</div>
		<div class="top-menu-mobile hide">
			<ul>
				<li>
					<a href="#" id="search-button"></a>
				</li>
				<li>
					<a href="#" id="online-button"></a>
				</li>
				<li>
					<a href="#" id="login-button"></a>
				</li>
				<li>
					<a href="#" id="menu-button"></a>
				</li>
			</ul>
		</div>
	</div>
	<div class="clear"></div>
	<div class="mega-menu">
		<ul>
			<li id="home-page">
				<a href="<?php echo $mainURL; ?>" title="Trang Chủ">
					<img src="<?php echo $mainURL; ?>/images/Icon-home.png" id="icon-home"/>
				</a>
			</li>
			<li class="list-megamenu" href="#Góc Nắng Khuya" title="Góc Nắng Khuya" alt="Góc Nắng Khuya">
				<a href="<?php echo $mainURL; ?>">
					Góc Nắng Khuya
				</a>
			</li>
			<li class="list-megamenu" >
				<a href="javascript:void(0);" title="Thể Loại" alt="Thể Loại">
					Thể Loại
				</a>
				<span class="click-sub-menu"></span>
				<div class="sub-menu">
					<span class="click-sub-sub-menu"></span>
					<?php
					$parentMenu=$process_menu->getListMenu();
					foreach($parentMenu as $_parent){
						$name=$_parent->getname();
						$seoLink=$superCore->toAscii($_parent->getname_ascii())."-".$_parent->getid().".html";
						$listChildren=$process_menu->getChildrenMenu($_parent->getid());
						if($name=="Radio Online"){
							continue;
						}
						?>
						<a href="theloai-<?php echo $seoLink ?>" class="list-sub-menu" title="<?php echo $name ?>"><?php echo $name ?></a>
						<?php if(count($listChildren) > 0){ ?>
							<?php foreach($listChildren as $_child){ ?>
								<?php
								$nameChild=$_child->getname();
								$seoLinkChildren=$_child->getname()."-".$_child->getid().".html";
								?>
								<a href="theloai-<?php echo $seoLinkChildren ?>" class="list-sub-menu-children" title="<?php echo $nameChild ?>"><?php echo $nameChild; ?></a>
							<?php } ?>
						<?php } ?>
						<?php
					}
					?>
				</div>
			</li>
			<li class="list-megamenu">
				<a href="javascript:void(0);" title="Nhạc Cụ" alt="Nhạc Cụ">
					Nhạc Cụ
				</a>
				<span class="click-sub-menu"></span>
				<div class="sub-menu nhac_cu">
					<span class="click-sub-sub-menu"></span>
					<?php
					$instrumentMusic=$process_menu->getListMusicInstrament();
					$breakLine=0;
					foreach($instrumentMusic as $_instrument){
					$name=$_instrument->getname();
					$seoLink=$superCore->toAscii($_instrument->getname_ascii())."-".$_instrument->getid().".html";
					if($breakLine % 5 ==0) { ?>
						<?php if($breakLine!=0){ ?>
							</div>
						<?php } ?>
							<div class="block-sub-menu">
					<?php } ?>
							<a href="instrument-<?php echo $seoLink; ?>" title="<?php echo $name ?>"><?php echo $name ?></a>
					<?php $breakLine++; ?>
					<?php } ?>
				</div>
			</li>
			<li class="list-megamenu">
				<a href="javascript:void(0);" title="Woim Radio Online" alt="Woim Radio Online">
					Radio Online
				</a>
				<span class="click-sub-menu"></span>
					<div class="sub-menu">
						<span class="click-sub-sub-menu"></span>
						<a href="/theloai-woim-radio-14.html" class="list-sub-menu" title="WOIM Radio Show" alt="WOIM Radio Show">WOIM Radio Show</a>
						<a href="/theloai-nghe-toi-ke-nay-15.html" class="list-sub-menu" title="Nu Radio" alt="Nu Radio">Nu Radio</a>
						<a href="/theloai-goc-chia-se-16.html" class="list-sub-menu" title="Góc Thành Viên" alt="Góc Thành Viên">Góc Thành Viên</a>
						<!--<a href="/theloai-Góc Chia Sẻ-16.html" class="list-sub-menu">Cafe Sáng</a>-->
					</div>
			</li>
			<li class="list-megamenu">
				<a href="#" title="Album Mới" alt="Album Mới">
					Album Mới
				</a>
			</li>
			<!--<li class="list-megamenu">
				<a href="#" title="Tạp Chí Woim">
					Tạp Chí Woim
				</a>
			</li>
			<li class="list-megamenu">
				<a href="#" title="Diễn Đàn">
					Diễn Đàn
				</a>
			</li>-->
		</ul>
	</div>
</header>
<div class="clear"></div>
<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery(".list-megamenu").hover(function(){
			var sub_menu=jQuery(this).find("ul");
			if(sub_menu.hasClass("active")){
				sub_menu.removeClass("active");
			}else{
				sub_menu.addClass("active");
			}
		});
	});
</script>
