<?php
$title=" Woim Nhạc Không Lời - Hòa Tấu - WOIM Radio Hay Nhất";
$metaKeyword="Woim,woim.nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
$metaDescription="Woim Nhạc Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>
<?php
				$action="";
				if(isset($_REQUEST["action"])){
					$action=$_REQUEST["action"];	
				}
				switch ($action) {
					case 'album':
						include_once("super_process_Homepage.php");
						$processHomepage = new Super_Process_HomePage("album","id");
						$id_detail=$_REQUEST["id_detail"];
						$currentDetailAlbum=$processHomepage->load($id_detail);
						$title=$currentDetailAlbum->getname();
						$arrayTags=$currentDetailAlbum->gettag_id();
						$metaDescription=$currentDetailAlbum->getinfo();
						$metaDescription="Thể loại Nhạc,Album $title Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
						$tags=$processHomepage->getListTag($arrayTags);
						$metaKeyword="";
						foreach($tags as $_tag){
							$metaKeyword.=$_tag->getname();
						}
						$metaKeyword.=",$title";
						$image=$superCore->getMediaUrl()."".$currentDetailAlbum->getcover();
						$image_src=$superCore->getMediaUrl()."".$currentDetailAlbum->getcover();
						$classRecent=Super_Core::getModel("recent","id_recent","Recent");
						$classRecent->addRecentByAlbum($id_detail,$user_id);
						break;
					case "detailAlbum":
						include_once("templates/detail-album.php");
						break;
					case "detailsong":
						include_once("super_process_song.php");
						$detailSong= new Super_Process_Song("song","id");
						$id_song=$superCore->getRequest("id");
						$currentSong=$detailSong->getDetailSong($id_song);
						$title=$currentSong[0]->getname();
						$metaDescription="Thể loại Nhạc $title Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
						$metaKeyword=$title.",nhạc không lời,nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
						$image_src=$superCore->getImgesUrl()."/default_album.png";
						$image=$superCore->getImgesUrl()."/default_album.png";
						$classRecent=Super_Core::getModel("recent","id_recent","Recent");
						$classRecent->addRecentBySong($id_song,$user_id);
						break;
					case "theloai":
						include_once("super_process_category.php");
						$category = new Super_Process_Category("category","id");
						$id_category=$superCore->getRequest("id");
						$currentCategory=$category->getDetailCategory($id_category);
						$title=$currentCategory[0]->getname();
						$des="";
						if($des==""){
						$metaDescription="Thể loại Nhạc $title  Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
						}
						$metaKeyword=$title." ,nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
						$image_src="$mainURL/images/home.png";
						break;
					case "nhaccu":
						include_once("super_process_nhaccu.php");
						$tag = new Super_Process_Nhaccu("tag","id");
						$id_tag=$superCore->getRequest("id");
						$currentTag=$tag->getDetailTag($id_tag);
						$title=$currentTag[0]->getname();
						$des="";
						if($des==""){
							$metaDescription="Thể loại Nhạc $title  Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
						}
						$metaKeyword=$title." ,nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
						$image_src="$mainURL/images/home.png";
						break;
					case "list-category-album":
						include_once("super_process_album.php");
						$dataAlbum = new Super_Process_Album("album","id");
						$id_category=$superCore->getRequest("id_list");
						$limit=16;
						$start=0;
						$end=0;
						$page=0;
						$beforeTra=0;
						$endTra=10;
						$listAlbum=$dataAlbum->getListAlbumBelongToCategory($id_category,0,0);
						$countPage=count($listAlbum);
						$endPage=intval($countPage/$limit);
						if(!isset($_REQUEST["page"])){
							$page=0;
							$beforeTra=0;
							$endTra=10;
							if($endPage < 10){
								if($endPage==0){
									$endPage=1;
								}
								$endTra=$endPage;

							}
							$_SESSION["dau"] = $beforeTra;
							$_SESSION["cuoi"] = $endTra;
						}else{
							$page=$superCore->getRequest("page");
							$beforeTra=$_SESSION["dau"];
							$endTra=$_SESSION["cuoi"];
							$vaoBefore=false;
							$vaoEnd=false;
							if($page < $endTra && $page > $beforeTra){
								$beforeTra=$_SESSION["dau"];
								$endTra=$_SESSION["cuoi"];
								$vaoEnd=true;
							}
							if($page==$beforeTra){
								$beforeTra=$page-10;
								$endTra=$page;
								$_SESSION["dau"]=$beforeTra;
								$_SESSION["cuoi"]=$endTra;
								if($beforeTra < 10){
									$beforeTra=0;
									$endTra=10;
									$_SESSION["dau"]=$beforeTra;
									$_SESSION["cuoi"]=$endTra;
								}
								$vaoBefore=true;
							}
							if($page==$endTra-1){
								if($vaoBefore==false || $vaoEnd==false) {
									$beforeTra = $page;
									$endTra = $page + 10;
									$_SESSION["dau"] = $beforeTra;
									$_SESSION["cuoi"] = $endTra;
									if ($endTra > $endPage) {
										$beforeTra = $endPage - 10;
										$endTra = $endPage;
										$_SESSION["dau"] = $beforeTra;
										$_SESSION["cuoi"] = $endTra;
									}
								}
							}
							if($page==0){
								$beforeTra=0;
								$endTra=10;
								if($endPage < 10){
									if($endPage==0){
										$endPage=1;
									}
									$endTra=$endPage;
								}
								$_SESSION["dau"] = $beforeTra;
								$_SESSION["cuoi"] = $endTra;

							}
							if($page==$endPage-1){
								$beforeTra=$endPage - 10;;
								$endTra=$endPage;
								if($endPage < 10){
									if($endPage==0){
										$endPage=1;
									}
									$beforeTra=0;
									$endTra=$endPage;
								}
								$_SESSION["dau"] = $beforeTra;
								$_SESSION["cuoi"] = $endTra;
							}
						}
						$start=$limit * $page;
						$end=$start + $limit;
						$listAlbum=$dataAlbum->getListAlbumBelongToCategory($id_category,$start,$limit);
						include_once("super_process_category.php");
						$category = new Super_Process_Category("category","id");
						$currentCategory=$category->getDetailCategory($id_category);
						$title=$currentCategory[0]->getname();
						$des="";
						if($des==""){
							$metaDescription="Thể loại Nhạc $title  Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
						}
						$metaKeyword=$title." ,nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
						$image_src="$mainURL/images/home.png";
						$seoLink=$superCore->toAscii($currentCategory[0]->getname_ascii());
						$urlCurrent="/the-loai-album-$seoLink-$id_category";
						$next=$page+1;
						if($page==0){
							$pre=0;
						}
						$pre=$page-1;
						$classActive="active-page";
						break;
					case "list-nhaccu-album":
						include_once("super_process_album.php");
						$dataAlbum = new Super_Process_Album("album","id");
						$id_category=$superCore->getRequest("id_list_tag");
						$limit=16;
						$start=0;
						$end=0;
						$page=0;
						$listAlbum=$dataAlbum->getListAlbumBelongToTag($id_category,0,0);
						$countPage=count($listAlbum);
						$endPage=intval($countPage/$limit);
						if(!isset($_REQUEST["page"])){
							$page=0;
							$beforeTra=0;
							$endTra=10;
							if($endPage < 10){
								if($endPage==0){
									$endPage=1;
								}
								$endTra=$endPage;

							}
							$_SESSION["dau"] = $beforeTra;
							$_SESSION["cuoi"] = $endTra;
						}else{
							$page=$superCore->getRequest("page");
							$beforeTra=$_SESSION["dau"];
							$endTra=$_SESSION["cuoi"];
							$vaoBefore=false;
							$vaoEnd=false;
							if($page < $endTra && $page > $beforeTra){
								$beforeTra=$_SESSION["dau"];
								$endTra=$_SESSION["cuoi"];
								$vaoEnd=true;
							}
							if($page==$beforeTra){
								$beforeTra=$page-10;
								$endTra=$page;
								$_SESSION["dau"]=$beforeTra;
								$_SESSION["cuoi"]=$endTra;
								if($beforeTra < 10){
									$beforeTra=0;
									$endTra=10;
									$_SESSION["dau"]=$beforeTra;
									$_SESSION["cuoi"]=$endTra;
								}
								$vaoBefore=true;
							}
							if($page==$endTra-1){
								if($vaoBefore==false || $vaoEnd==false) {
									$beforeTra = $page;
									$endTra = $page + 10;
									$_SESSION["dau"] = $beforeTra;
									$_SESSION["cuoi"] = $endTra;
									if ($endTra > $endPage) {
										$beforeTra = $endPage - 10;
										$endTra = $endPage;
										$_SESSION["dau"] = $beforeTra;
										$_SESSION["cuoi"] = $endTra;
									}
								}
							}
							if($page==0){
								$beforeTra=0;
								$endTra=10;
								if($endPage < 10){
									if($endPage==0){
										$endPage=1;
									}
									$endTra=$endPage;
								}
								$_SESSION["dau"] = $beforeTra;
								$_SESSION["cuoi"] = $endTra;

							}
							if($page==$endPage-1){
								$beforeTra=$endPage - 10;;
								$endTra=$endPage;
								if($endPage < 10){
									if($endPage==0){
										$endPage=1;
									}
									$beforeTra=0;
									$endTra=$endPage;
								}
								$_SESSION["dau"] = $beforeTra;
								$_SESSION["cuoi"] = $endTra;
							}
						}
						$start=$limit * $page;
						$end=$start + $limit;
						$listAlbum=$dataAlbum->getListAlbumBelongToTag($id_category,$start,$limit);
						include_once("super_process_nhaccu.php");
						$category = new Super_Process_Nhaccu("tag","id");
						$currentCategory=$category->getDetailTag($id_category);
						$title=$currentCategory[0]->getname();
						$des="";
						if($des==""){
							$metaDescription="Thể loại Nhạc $title  Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
						}
						$metaKeyword=$title." ,nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
						$image_src="$mainURL/images/home.png";
						$seoLink=$superCore->toAscii($currentCategory[0]->getname_ascii());
						$urlCurrent="/nhac-cu-album-$seoLink-$id_category";
						$next=$page+1;
						if($page==0){
							$pre=0;
						}
						$pre=$page-1;
						$classActive="active-page";
						break;
					case "list-category-song":
						include_once("super_process_album.php");
						$dataAlbum = new Super_Process_Album("album","id");
						$id_category=$superCore->getRequest("id_list");
						$limit=16;
						$start=0;
						$end=0;
						$page=0;
						$listSong=$dataAlbum->getListSongBelongToCategory($id_category,0,0);
						$countPage=count($listSong);
						$endPage=intval($countPage/$limit);
						if(!isset($_REQUEST["page"])){
							$page=0;
							$beforeTra=0;
							$endTra=10;
							if($endPage < 10){
								if($endPage==0){
									$endPage=1;
								}
								$endTra=$endPage;

							}
							$_SESSION["dau"] = $beforeTra;
							$_SESSION["cuoi"] = $endTra;
						}else{
							$page=$superCore->getRequest("page");
							$beforeTra=$_SESSION["dau"];
							$endTra=$_SESSION["cuoi"];
							$vaoBefore=false;
							$vaoEnd=false;
							if($page < $endTra && $page > $beforeTra){
								$beforeTra=$_SESSION["dau"];
								$endTra=$_SESSION["cuoi"];
								$vaoEnd=true;
							}
							if($page==$beforeTra){
								$beforeTra=$page-10;
								$endTra=$page;
								$_SESSION["dau"]=$beforeTra;
								$_SESSION["cuoi"]=$endTra;
								if($beforeTra < 10){
									$beforeTra=0;
									$endTra=10;
									$_SESSION["dau"]=$beforeTra;
									$_SESSION["cuoi"]=$endTra;
								}
								$vaoBefore=true;
							}
							if($page==$endTra-1){
								if($vaoBefore==false || $vaoEnd==false) {
									$beforeTra = $page;
									$endTra = $page + 10;
									$_SESSION["dau"] = $beforeTra;
									$_SESSION["cuoi"] = $endTra;
									if ($endTra > $endPage) {
										$beforeTra = $endPage - 10;
										$endTra = $endPage;
										$_SESSION["dau"] = $beforeTra;
										$_SESSION["cuoi"] = $endTra;
									}
								}
							}
							if($page==0){
								$beforeTra=0;
								$endTra=10;
								if($endPage < 10){
									if($endPage==0){
										$endPage=1;
									}
									$endTra=$endPage;
								}
								$_SESSION["dau"] = $beforeTra;
								$_SESSION["cuoi"] = $endTra;

							}
							if($page==$endPage-1){
								$beforeTra=$endPage - 10;;
								$endTra=$endPage;
								if($endPage < 10){
									if($endPage==0){
										$endPage=1;
									}
									$beforeTra=0;
									$endTra=$endPage;
								}
								$_SESSION["dau"] = $beforeTra;
								$_SESSION["cuoi"] = $endTra;
							}
						}
						$start=$limit * $page;
						$end=$start + $limit;
						$listSong=$dataAlbum->getListSongBelongToCategory($id_category,$start,$limit);
						include_once("super_process_category.php");
						$category = new Super_Process_Category("category","id");
						$currentCategory=$category->getDetailCategory($id_category);
						$title=$currentCategory[0]->getname();
						$des="";
						if($des==""){
							$metaDescription="Thể loại Nhạc $title  Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
						}
						$metaKeyword=$title." ,nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
						$image_src="$mainURL/images/home.png";
						$urlCurrent="/the-loai-song-$title-$id_category";
						$next=$page+1;
						if($page==0){
							$pre=0;
						}
						$pre=$page-1;
						$classActive="active-page";
						break;
					case "list-nhaccu-song":
						include_once("super_process_album.php");
						$dataAlbum = new Super_Process_Album("album","id");
						$id_category=$superCore->getRequest("id_list_tag");
						$limit=16;
						$start=0;
						$end=0;
						$page=0;
						$listSong=$dataAlbum->getListSongBelongToTag($id_category,0,0);
						$countPage=count($listSong);
						$endPage=intval($countPage/$limit);
						if(!isset($_REQUEST["page"])){
							$page=0;
							$beforeTra=0;
							$endTra=10;
							if($endPage < 10){
								if($endPage==0){
									$endPage=1;
								}
								$endTra=$endPage;

							}
							$_SESSION["dau"] = $beforeTra;
							$_SESSION["cuoi"] = $endTra;
						}else{
							$page=$superCore->getRequest("page");
							$beforeTra=$_SESSION["dau"];
							$endTra=$_SESSION["cuoi"];
							$vaoBefore=false;
							$vaoEnd=false;
							if($page < $endTra && $page > $beforeTra){
								$beforeTra=$_SESSION["dau"];
								$endTra=$_SESSION["cuoi"];
								$vaoEnd=true;
							}
							if($page==$beforeTra){
								$beforeTra=$page-10;
								$endTra=$page;
								$_SESSION["dau"]=$beforeTra;
								$_SESSION["cuoi"]=$endTra;
								if($beforeTra < 10){
									$beforeTra=0;
									$endTra=10;
									$_SESSION["dau"]=$beforeTra;
									$_SESSION["cuoi"]=$endTra;
								}
								$vaoBefore=true;
							}
							if($page==$endTra-1){
								if($vaoBefore==false || $vaoEnd==false) {
									$beforeTra = $page;
									$endTra = $page + 10;
									$_SESSION["dau"] = $beforeTra;
									$_SESSION["cuoi"] = $endTra;
									if ($endTra > $endPage) {
										$beforeTra = $endPage - 10;
										$endTra = $endPage;
										$_SESSION["dau"] = $beforeTra;
										$_SESSION["cuoi"] = $endTra;
									}
								}
							}
							if($page==0){
								$beforeTra=0;
								$endTra=10;
								if($endPage < 10){
									if($endPage==0){
										$endPage=1;
									}
									$endTra=$endPage;
								}
								$_SESSION["dau"] = $beforeTra;
								$_SESSION["cuoi"] = $endTra;

							}
							if($page==$endPage-1){
								$beforeTra=$endPage - 10;;
								$endTra=$endPage;
								if($endPage < 10){
									if($endPage==0){
										$endPage=1;
									}
									$beforeTra=0;
									$endTra=$endPage;
								}
								$_SESSION["dau"] = $beforeTra;
								$_SESSION["cuoi"] = $endTra;
							}
						}
						$start=$limit * $page;
						$end=$start + $limit;
						$listSong=$dataAlbum->getListSongBelongToTag($id_category,$start,$limit);
						include_once("super_process_nhaccu.php");
						$category = new Super_Process_Nhaccu("tag","id");
						$currentCategory=$category->getDetailCategory($id_category);
						$title=$currentCategory[0]->getname();
						$des="";
						if($des==""){
							$metaDescription="Thể loại Nhạc $title  Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
						}
						$metaKeyword=$title." ,nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
						$image_src="$mainURL/images/home.png";
						$urlCurrent="/the-loai-song-$title-$id_category";
						$next=$page+1;
						if($page==0){
							$pre=0;
						}
						$pre=$page-1;
						$classActive="active-page";
						break;
					case "search":
						$keywordSearch=$_REQUEST["q"];
						$conditionSearch=$_REQUEST["c"];
						$keywordSearch=mysql_escape_string($keywordSearch);
						$conditionSearch=mysql_escape_string($conditionSearch);
						$dataSearch=$superCore::getModel("category","id","Category");
						$listAlbum=null;
						$listSong=null;
						switch ($conditionSearch){
							case "album":
								$listAlbum=$dataSearch->getAlbumBySearch($keywordSearch,0,0);
								break;
							case "song":
								$listAlbum=$dataSearch->getSongBySearch($keywordSearch,0,0);
								break;
							case "nghesi":
								$listAlbum=$dataSearch->getAlbumBySearchArtist($keywordSearch,0,0);
								//$listSong=$dataSearch->getSongBySearchArtist($keywordSearch);
								break;

						}
						$limit=16;
						$start=0;
						$end=0;
						$page=0;
						$beforeTra=0;
						$endTra=10;
						$countPage=count($listAlbum);
						$endPage=intval($countPage/$limit);
						if(!isset($_REQUEST["page"])){
							$page=0;
							$beforeTra=0;
							$endTra=10;
							if($endPage < 10){
								if($endPage==0){
									$endPage=1;
								}
								$endTra=$endPage;

							}
							$_SESSION["dau"] = $beforeTra;
							$_SESSION["cuoi"] = $endTra;
						}else{
							$page=$superCore->getRequest("page");
							$beforeTra=$_SESSION["dau"];
							$endTra=$_SESSION["cuoi"];
							$vaoBefore=false;
							$vaoEnd=false;
							if($page < $endTra && $page > $beforeTra){
								$beforeTra=$_SESSION["dau"];
								$endTra=$_SESSION["cuoi"];
								$vaoEnd=true;
							}
							if($page==$beforeTra){
								$beforeTra=$page-10;
								$endTra=$page;
								$_SESSION["dau"]=$beforeTra;
								$_SESSION["cuoi"]=$endTra;
								if($beforeTra < 10){
									$beforeTra=0;
									$endTra=10;
									$_SESSION["dau"]=$beforeTra;
									$_SESSION["cuoi"]=$endTra;
								}
								$vaoBefore=true;
							}
							if($page==$endTra-1){
								if($vaoBefore==false || $vaoEnd==false) {
									$beforeTra = $page;
									$endTra = $page + 10;
									$_SESSION["dau"] = $beforeTra;
									$_SESSION["cuoi"] = $endTra;
									if ($endTra > $endPage) {
										$beforeTra = $endPage - 10;
										$endTra = $endPage;
										$_SESSION["dau"] = $beforeTra;
										$_SESSION["cuoi"] = $endTra;
									}
								}
							}
							if($page==0){
								$beforeTra=0;
								$endTra=10;
								if($endPage < 10){
									if($endPage==0){
										$endPage=1;
									}
									$endTra=$endPage;
								}
								$_SESSION["dau"] = $beforeTra;
								$_SESSION["cuoi"] = $endTra;

							}
							if($page==$endPage-1){
								$beforeTra=$endPage - 10;;
								$endTra=$endPage;
								if($endPage < 10){
									if($endPage==0){
										$endPage=1;
									}
									$beforeTra=0;
									$endTra=$endPage;
								}
								$_SESSION["dau"] = $beforeTra;
								$_SESSION["cuoi"] = $endTra;
							}
						}
						$start=$limit * $page;
						$end=$start + $limit;
						$Link="";
						$titleSearch="";
						switch ($conditionSearch){
							case "album":
								$listAlbum=$dataSearch->getAlbumBySearch($keywordSearch,$start,$limit);
								$Link="album-";
								$titleSearch="Album";
								break;
							case "song":
								$listAlbum=$dataSearch->getSongBySearch($keywordSearch,$start,$limit);
								$Link="";
								$titleSearch="Bài Hát";
								break;
							case "nghesi":
								$listAlbum=$dataSearch->getAlbumBySearchArtist($keywordSearch,$start,$limit);
								$titleSearch="Nghệ Sĩ";
								//$listSong=$dataSearch->getSongBySearchArtist($keywordSearch);
								break;

						}
						$title=$keywordSearch;
						$des="";
						$metaDescription="Thể loại Nhạc,Album $title  Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
						$metaKeyword=$title." ,nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar,album";
						$image_src="$mainURL/images/home.png";
						$urlCurrent="/search-$title-$conditionSearch";
						$next=$page+1;
						if($page==0){
							$pre=0;
						}
						$pre=$page-1;
						$classActive="active-page";
						break;
					case "artist":
						include_once("super_process_artist.php");
						$artist= new Super_Process_Artist("artist","id");
						$id_artist=$superCore->getRequest("id_artist");
						$currentArtist=$artist->getDetailArtist($id_artist);
						$title=$currentArtist[0]->getname();
						$des="";
						if($des==""){
							$metaDescription="Nhạc Sĩ $title của dòng nhạc Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
						}
						$metaKeyword=$title.",nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
						$image_src=$superCore->getImgesUrl()."/default_profile.jpg";
						break;
					case "tim_kiem_nang_cao":
						$image_src="$mainURL/images/home.png";
						$image="$mainURL/images/home.png";
						break;
					case "logout":
						$image_src="$mainURL/images/home.png";
						$image="$mainURL/images/home.png";
						break;
					default:
						$image_src="$mainURL/images/home.png";
						$image="$mainURL/images/home.png";
						break;
				}
$metaDescription.=" Woim ";
$metaKeyword.=",Woim";
?>
<title><?php echo $title; ?></title>
<meta name="viewport" content="width=device-width">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="robots" content="INDEX,FOLLOW" />
<link rel="author" href="http://woim.me" />
<meta name="google-site-verification" content="google0c14b4672ba35961" />
<meta name="y_key" content="" />
<meta name="keywords" content="<?php echo $metaKeyword ?>" />
<meta name="description" content="<?php echo $metaDescription ?>" />
<meta property="og:title" content="<?php echo $title ?>" />
<meta content="<?php echo $metaDescription ?>" property="og:description"> 
<meta property="og:image:type" content="image/jpeg">
<link rel="image_src" href="<?php echo $image_src; ?>" / >
<meta property="og:image" content="<?php echo $image ?>" />
<meta property="og:site_name" content="<?php echo $title ?>" />
<meta property="og:url" content="<?php echo $url ?>">
<link rel="canonical" href="<?php echo $url  ?>" />