<?php
include_once("Collection.php");
class Super_Process_Recent extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
    function getListRecent($user_id){
        $sql = "select * from recent where user_id='$user_id'";
        $data = $this->getCollectionBySql($sql);
        return $data;
    }
    function getSongRecent($arrayIdSong){
        $sql="select * from album where id in ($arrayIdSong)";
        $data = $this->getCollectionBySql($sql);
        return $data;
    }
    function checkExsitSong($id_song,$user_id){
        $sql = "select * from recent where user_id='$user_id' and song_id='$id_song'";
        $data = $this->getCollectionBySql($sql);
        return $data;
    }
    function checkExsitAlbum($id_album,$user_id){
        $sql = "select * from recent where user_id='$user_id' and album_id='$id_album'";
        $data = $this->getCollectionBySql($sql);
        return $data;
    }
    function getListMenu(){
        $sql="select * from category where parent=0";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getChildrenMenu($parent_id){
        $sql="select * from category where parent=$parent_id";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getDetailCategory($id_category){
        $sql="select * from category where id=$id_category";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getAlbumInCategory($id){
        $sql="select * from album where category_id=$id order by id desc limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getSongInCategory($id_category){
        $sql="select * from song where category_id=$id_category order by id desc limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getArtistBySongId($artist_id){
        $sql="select * from artist where id=$artist_id";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getAlbumOfSong($album_id){
        $sql="select name,id from album where id=$album_id";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getAlbumByArtist($artist_id){
        $sql="select * from album where artist_id=$artist_id limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getSongByArtist($artist_id){
        $sql="select * from song where artist_id=$artist_id limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getCommentOnSong($song_id){
        $sql="select content,username from song_comment where song_id=$song_id limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function addRecentBySong($id_song,$user_id){
        $dataSongRecent=$this->checkExsitSong($id_song,$user_id);
        if(count($dataSongRecent) < 1){
            $this->setData("id_recent","NULL");
            $this->setData("song_id",$id_song);
            $this->setData("user_id",$user_id);
            if($this->inserRow() > 0){
            }else{
            }
        }else{
            $id_recent=$dataSongRecent[0]->getid_recent();
            $countPress=$dataSongRecent[0]->getcount_press();
            $countPress=$countPress+1;
            $this->setData("id_recent",$id_recent);
            $this->setData("count_press",$countPress);
            $this->updateRow();
        }
    }
    function addRecentByAlbum($id_album,$user_id){
        $dataAlbum=$this->checkExsitAlbum($id_album,$user_id);
        if(count($dataAlbum) < 1) {
            $this->setData("id_recent", "NULL");
            $this->setData("album_id", $id_album);
            $this->setData("user_id", $user_id);
            if ($this->inserRow() > 0) {

            } else {

            }
        }else{
            $id_recent=$dataAlbum[0]->getid_recent();
            $countPress=$dataAlbum[0]->getcount_press();
            $countPress=$countPress+1;
            $this->setData("id_recent",$id_recent);
            $this->setData("count_press",$countPress);
            $this->updateRow();
        }
    }
}