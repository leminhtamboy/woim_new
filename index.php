<!-- index -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<?php include_once("head.php"); ?>
<body>
<?php include_once("process_notice.php"); ?>
<!--<div class="notice" style="width:100%">
	<p style="color:white">Mừng các bạn đã trở lại với woim! Hãy để lại email và nhấn subscribe để nhận thông báo khi có số radio mới và album được cập nhật.</p>
	<form id="subcribe_form" name="subcribe_form" method="post" action="">
		<input type="text" style="color:black" value="" placeholder="Email" name="clien-email"/>
		<input type="submit" style="color:white" value="Subscribe" name="submit_subcribe" />
	</form>
	<p style="color:white">Website đang trong giai đoạn xây dựng , nên sẽ có một vài sự cố , mong các bạn thông cảm . Woim sẽ khắc phục trong thời gian sớm nhất . Chúc các bạn nghe nhạc vui vẻ.</p>
</div>-->
<?php include_once("header.php"); ?>
<!-- content -->
<div id="main">
	<div class="mainContent">
		<div class="leftContent">
			<?php
				$action="";
				if(isset($_REQUEST["action"])){
					$action=$_REQUEST["action"];	
				}
				switch ($action) {
					case 'album':
						include_once("templates/detail-album.php");
						break;
					case 'song':
						include_once("templates/detail-song.php");
						break;
					case "detailAlbum":
						include_once("templates/detail-album.php");
						break;
					case 'search':
						include_once("templates/search.php");
						break;
					case "detailsong":
						include_once("templates/detail-song.php");
						break;
					case "theloai":
						include_once("templates/detail-category.php");
						break;
					case "nhaccu":
						include_once("templates/detail-tag.php");
						break;
					case "list-category-album":
						include_once("templates/list-album.php");
						break;
					case "list-category-song":
						include_once("templates/list-song.php");
						break;
					case "list-nhaccu-album":
						include_once("templates/list-album.php");
						break;
					case "list-nhaccu-song":
						include_once("templates/list-song.php");
						break;
					case "artist":
						include_once("templates/detail-artist.php");
						break;
					case "dang_ki_thanh_vien":
						include_once("templates/dang-ki-thanh-vien.php");
						break;
					case "quen_mat_khau":
						include_once("templates/quen-mat-khau.php");
						break;
					case "doi_mat_khau":
						include_once("templates/doi-mat-khau.php");
						break;
					case "user-album-like":
						include_once("templates/user-album-like.php");
						break;
					case "profile-user":
						include_once("templates/profile-user-album-like.php");
						break;
					case "tim_kiem_nang_cao":
						include_once("templates/tim-kiem-nang-cao.php");
						break;
					case "logout":
						include_once("templates/logout.php");
						break;
					default:
						include_once("templates/home-page.php");
						break;
				}
			 ?>
			
		</div>
		<div class="rightContent">
		<?php include_once("templates/right.php"); ?>
		</div>
		<div class="clear"></div>
		<div id='phan_loai'>
				<?php 
				switch ($action) {
					case 'album':
						include_once("templates/phan-loai-chung.php");
						break;
					case 'song':
						include_once("templates/phan-loai-song.php");
						break;
					case "detailAlbum":
						include_once("templates/phan-loai-album.php");
						break;
					case "detailsong":
						//include_once("templates/phan-loai-song.php");
						include_once("templates/phan-loai-chung.php");
						break;
					case "theloai":
						include_once("templates/phan-loai-chung.php");
						//include_once("templates/phan-loai-category.php");
						break;
					case "artist":
						include_once("templates/phan-loai-chung.php");
						break;
					default:
						include_once("templates/phan-loai-chung.php");
						//include_once("templates/phan-loai-suggested.php");
						break;
				}
				?>
		</div>
	</div>
</div>
<?php ?>
<!-- end content -->
</body>
<?php include_once("footer.php"); ?>
<div class="overlay"></div>
</html>
