<?php
include_once("Collection.php");
class Super_Process_Pageextends Collection{
	var $start=0;
	var $limit=10; //default
	var $end=10; //defautl
	var $_tableSource="NULL";
	var $_countPage=0;
    function __construct($tableName,$primaryKey,$_limit=10){
        parent::__construct($tableName,$primaryKey);
        $this->_tableSource=$tableName;
        $this->limit=$_limit;
        $allPage=$this->getCountData($primaryKey);
        $this->_countPage=intval($allPage/$_limit);
    }
    function getCountData($primary_key){
    	$sql="select count($primary_key) as countdata from `".$this->_tableSource."`";
    	$data=$this->getCollectionBySql($sql);
        return $data[0]->getcountdata();
    }
    function getPage($currentPage=1){
    	if($currentPage==1){
    		$this->start=0;
    		$this->end=$this->limit;
    	}else{
    		$this->end=$currentPage * ($this->limit +1);
    		$this->start=($this->limit+1);
    	}
    	$sql="select * from `".$this->_tableSource."` limit $this->start , $this->end ";
    	$data=$this->getCollectionBySql($sql);
        return $data;
    }
    function bindingSort($condition){
    	//do something
    }
    function drawSelectPagination(){
    	//do something
    }
	
}