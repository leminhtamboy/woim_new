<?php
include_once("Database.php");
class Super_Abstract extends Database
{
    //protected $_collection=null;
    protected $_tableName=null;
    protected $_primaryKey=null;
    protected $_db=null;
    public $_sql="";

    function __construct($tableName,$primaryKey)
    {
        $this->_tableName=$tableName;
        $this->_primaryKey=$primaryKey;
        $this->_db = new Database();
    }

    // temp query
    protected function _query(){
        $this->_sql="select * from $this->_tableName";
        return $this->_db->ExcuteObjectList($this->_sql);
    }
    protected  function _byQuery($__sql){
        $this->_sql=$__sql;
        return $this->_db->ExcuteObjectList($this->_sql);
    }
    //end temp query
    function getAbstractCollection(){
        $objects=$this->_query();
        return $objects;
    }
    function getAbstractCustomCollection($__sql){
        $objects=$this->_byQuery($__sql);
        return $objects;
    }
    function getLoadCollectionByPrimaryKey($value){
        $customSql="select * from $this->_tableName where $this->_primaryKey='$value'";
        $objects=$this->_byQuery($customSql);
        return $objects;
    }
    function getLoadCollectionByAttribute($columnName,$condition="eq",$value){
        $customSql="";
        switch ($condition){
            case "eq":
                $customSql="select * from $this->_tableName where `$columnName`='$value'";
                break;
            case "neq":
                $customSql="select * from $this->_tableName where `$columnName`!='$value'";
                break;
            case "like":
                $customSql="select * from $this->_tableName where `$columnName`like '%$value%'";
                break;
        }
        $objects=$this->_byQuery($customSql);
        return $objects;
    }
    function getLoadCollectionByAttributeArray($arrColumnName,$arrCondition="eq",$arrValue){
        $countColumn=count($arrColumnName);
        $customSql = "select * from $this->_tableName where 1=1 ";
        for($c=0;$c<$countColumn;$c++) {
            $columnName=$arrColumnName[$c];
            $condition=$arrCondition[$c];
            $value=$arrValue[$c];
            switch ($condition) {
                case "eq":
                    $customSql .= " AND `$columnName`='$value'";
                    break;
                case "neq":
                    $customSql .= " AND `$columnName`!='$value'";
                    break;
                case "like":
                    $customSql .= " AND `$columnName`like '%$value%'";
                    break;
            }
        }
        $objects=$this->_byQuery($customSql);
        return $objects;
    }
    function insertDB($arrayKey,$arrayValue){
        $sqlInsert="INSERT INTO $this->_tableName ";
        $key=0;
        $coutnKey=count($arrayKey);
        $sqlInsert.="(";
        for($key=0;$key<$coutnKey;$key++){
            if($key==$coutnKey-1){
                $sqlInsert.="`".str_replace('\'','',$arrayKey[$key]) ."`";
            }else{
                $sqlInsert.="`".str_replace('\'','',$arrayKey[$key]) ."`,";
            }

        }
        $sqlInsert.=") VALUES";
        $val=0;
        $coutnVal=count($arrayValue);
        $sqlInsert.="(";
        for($val=0;$val<$coutnVal;$val++){
            if($val==$coutnVal-1){
                $sqlInsert.="'".str_replace('\'','',$arrayValue[$val]) ."'";
            }else{
                $sqlInsert.="'".str_replace('\'','',$arrayValue[$val]) ."',";
            }

        }
        $sqlInsert.=");";
        $numRow=$this->_db->ExcuteNonQuery($sqlInsert);
        return $numRow;
    }
    function updateDB($arrayKey,$arrayValue){
        $sqlInsert="UPDATE $this->_tableName ";
        $key=0;
        $coutnKey=count($arrayKey);
        $sqlInsert.="SET ";
        for($key=1;$key<$coutnKey;$key++){
            if($key==$coutnKey-1){
                $sqlInsert.="`".str_replace('\'','',$arrayKey[$key]) ."`='".$arrayValue[$key]."'";
            }else{
                $sqlInsert.="`".str_replace('\'','',$arrayKey[$key]) ."`='".$arrayValue[$key]."',";
            }

        }
        $sqlInsert.=" WHERE $this->_primaryKey='$arrayValue[0]'";
        $numRow=$this->_db->ExcuteNonQueryUpdate($sqlInsert);
        return $numRow;
    }
    function deleteDB($id){
        $sqlDelete="DELETE from $this->_tableName where $this->_primaryKey='$id'";
        $numRow=$this->_db->ExcuteNonQueryDelete($sqlDelete);
        return $numRow;
    }
    function deleteBySql($sql){
        $numRow=$this->_db->ExcuteNonQueryDelete($sql);
        return $numRow;
    }
}