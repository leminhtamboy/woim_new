<?php
include_once("Collection.php");
class Super_Process_History extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
    function ListHistory(){
        $sql="select * from history order by desc limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function addHistoryBySong($content,$username,$userid){
        $this->setData("id_history","NULL");
        $this->setData("content",$content);
        $this->setData("username",$username);
        $this->setData("user_id",$userid);
        if($this->inserRow() > 0){
            echo "added";
        }else{
            echo "fail";
        }
    }
}