<?php
include_once("../Config.php");
include_once ("../head.php");
include_once ("../super_process_Homepage.php");
$superCore = new Super_Core();
$rootWeb=$superCore->getRootWeb();
$processHomepage = new Super_Process_HomePage("album","id");
$radio_online_slider_left=$processHomepage->getRadioOnline();
?>
<ul id="slider-album-new">
			<?php foreach($radio_online_slider_left as $radio) { ?>
    <?php
    $imgUrl=$rootWeb."/data/".$radio->getcover();
    if(!$superCore->checkIssetImage($imgUrl)){
        $imgUrl=$superCore->getImgesUrl()."/default_album.png";
    }
    $name=$radio->getname();
    $trich_dan=$radio->gettrich_dan();
    if($trich_dan==""){
        $trich_dan="Woim Radio Show góc bình yên tâm hồn";
    }
    ?>
    <li>
        <div class="img-slider">
            <img class="img-slider-detail" src="<?php echo $imgUrl; ?>"/>
        </div>
        <div class="title-slider">
            <a href="/album-<?php echo $superCore->toAscii($radio->getname_ascii()); ?>-<?php echo $radio->getid(); ?>.html" alt="<?php echo $name ?>" title="<?php echo $name; ?>">
                <h1 class="title-music"><?php echo $superCore::subStringLimit(35,$name); ?></h1>
            </a>
            <h3 class="short-description-music"><?php echo $trich_dan;  ?></h3>
        </div>
    </li>
<?php } ?>
</ul>
<script type="text/javascript">
    var owl_slider_album_new=jQuery("#slider-album-new");
    owl_slider_album_new.owlCarousel({
        items: 1,
        autoPlay: true,
        pagination: false,
        autoPlaySpeed: 4000,
        autoPlayTimeout: 4000,
        autoPlayHoverPause: true,
        itemsDesktop : [1000,1], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,1], // betweem 900px and 601px
        itemsTablet: [600,1], //2 items between 600 and 0
        itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    });
</script>