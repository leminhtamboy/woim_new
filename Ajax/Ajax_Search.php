<?php
$input_search=$_POST["input_search"];
$condition_search=$_POST["condition"];
include_once("../Collection.php");
include_once("../Config.php");
include_once("../Super.php");
$superCore = new Super_Core();
$album = new Collection("album","id");
$sql="select * from album where name like '%$input_search%' order by id desc limit 0,10";
$data=$album->getCollectionBySql($sql);
$urlImage=Super_Core::getMediaUrlStatic();
if($input_search != "") {
    ?>
    <ul class="box-search">
        <?php
        foreach ($data as $_album) {
            $imgUrl = $urlImage . "" . $_album->getcover();
            if (!$superCore->checkIssetImage($imgUrl)) {
                $imgUrl = $superCore->getImgesUrl() . "/default_album.png";
            }
            ?>
            <li class="li-search">
                <a href="/album-<?php echo $superCore->toAscii($_album->getname_ascii()); ?>-<?php echo $_album->getid(); ?>.html"
                   title="<?php echo $_album->getname(); ?>">
                    <img src="<?php echo $imgUrl ?>" class="img-search-ajax" alt="<?php echo $_album->getname(); ?>"/>
                    <h1 class="title-search"><?php echo $superCore::subStringLimit(45, $_album->getname()); ?></h1>
                </a>
            </li>
            <?php
        }
        ?>
    </ul>
    <?php
}else{
    echo "";
}
?>