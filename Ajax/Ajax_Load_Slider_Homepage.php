<?php
include_once("../Config.php");
include_once("../Super.php");
include_once("../head.php");
include_once ("../super_process_Homepage.php");
$superCore = new Super_Core();
$rootWeb=$superCore->getRootWeb();
$processHomepage = new Super_Process_HomePage("album","id");
$listAlbum = $processHomepage->getListAlbumMostView();
$listAlbumRecent=$processHomepage->getAlbumRecent($user_id);
?>
<ul id="homepage-suggested-album">
    <?php foreach ($listAlbum as $_album){?>
        <li class="list-sliders-li">
            <?php
            $imgUrl=$rootWeb."/data/".$_album->getcover();
            if(!$superCore->checkIssetImage($imgUrl)){
                $imgUrl=$superCore->getImgesUrl()."/default_album.png";
            }
            ?>
            <a href="/album-<?php echo $superCore->toAscii($_album->getname_ascii()); ?>-<?php echo $_album->getid(); ?>.html" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-sliders-href">
                <img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider">
                <h1><?php echo $_album->getname(); ?></h1>
            </a>
        </li>
    <?php } ?>
    <?php foreach ($listAlbumRecent as $_album){ break; ?>
        <li class="list-sliders-li">
            <?php
            $imgUrl=$rootWeb."/data/".$_album->getcover();
            if(!$superCore->checkIssetImage($imgUrl)){
                $imgUrl=$superCore->getImgesUrl()."/default_album.png";
            }
            ?>
            <a href="/album-<?php echo $superCore->toAscii($_album->getname_ascii()); ?>-<?php echo $_album->getid(); ?>.html" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-sliders-href">
                <img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider">
                <h1><?php echo $_album->getname(); ?></h1>
            </a>
        </li>
    <?php } ?>
</ul>
<script>
    var owl_also_like =  jQuery("#homepage-suggested-album");
    owl_also_like.owlCarousel({
        pagination: false,
        items : 4, //10 items above 1000px browser width
        autoPlay: true,
        itemsDesktop : [1000,4], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,2], // betweem 900px and 601px
        itemsTablet: [480,1], //2 items between 600 and 0
        itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    });
</script>
