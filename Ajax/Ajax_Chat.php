<?php
include_once ("../super_process_chat.php");
if($_POST)
{
    $chat= new Super_Process_Chat("shout_box","id");
    //connect to mysql db
    //check if its an ajax request, exit if not
    if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    }

    if(isset($_POST["message"]) &&  strlen($_POST["message"])>0)
    {
        //sanitize user name and message received from chat box
        //You can replace username with registerd username, if only registered users are allowed.
        $message=htmlspecialchars($_POST["message"]);
        if(count(explode("script",$message)) > 1){
            header('HTTP/1.1 500 Bo ngay cai tro nay di khong lam dc dau !!!');
            exit();
        }
        $username =$_POST["username"];
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $msg_time = date('h:i A M d',time()); // current time
        $user_id=$_POST["user_id"];
        $chat->insertChat($username,$message,$msg_time,$user_ip,$user_id);
        //insert new message in db
        echo '<div class="shout_msg"><time>'.$msg_time.'</time><span class="username"><a href="/profile-user-'.$user_id.'.html" title="'.$username.'">'.$username.'</a></span><span class="message">'.$message.'</span></div>';
        // delete all records except last 10, if you don't want to grow your db size!
        //mysqli_query($sql_con,"DELETE FROM shout_box WHERE id NOT IN (SELECT * FROM (SELECT id FROM shout_box ORDER BY id DESC LIMIT 0, 10) as sb)");
    }
    elseif($_POST["fetch"]==1)
    {
        $chat->showMessage();
    }
    else
    {
        header('HTTP/1.1 500 Are you kiddin me?');
        exit();
    }
}