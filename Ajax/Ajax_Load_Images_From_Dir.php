<?php
include_once("../Config.php");
$config = new ConfigGlobal();
include_once("../Super.php");
$superCore = new Super_Core();
$file_goc=$_POST["dir"];
$dir = $superCore->getDir()."/cdn_image_collection/Images/".$file_goc."/";
// Open a directory, and read its contents
$imgLoading=$superCore->getImgesUrl() . "/loading-animation.gif";
?>
<ul id="result-list">
<?php
if (is_dir($dir)){
    if ($dh = opendir($dir)){
        while (($file = readdir($dh)) !== false){
            if($file=="." || $file==".."){
                continue;
            }
            $imgUrl=$superCore->getWebUrl()."/cdn_image_collection/Images/".$file_goc."/".basename($file);
            ?>
            <li class="list-sliders-li-hot">
                <img src="<?php echo $imgLoading; ?>" onclick="reviewImageCover(this)" class="img-load-ding" data-url="<?php echo $imgUrl ?>" width="200px" height="200px"/>
            </li>
            <?php
        }
        closedir($dh);
    }
}
?>
</ul>
