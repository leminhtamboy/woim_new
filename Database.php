<?php
//class su dung phuong thuc ket noi cua pdo
class Database{
    private static $_hostname="127.0.0.1";
    private static $_username="root";
    private static $_password="";
    private static $connect=null;
    var $_adapter=null;
    private  static $_dbName="woim";
    var $_connectPDO=null;
    var $_stnt=null;
    public static function getInstance() {

        if (!self::$connect){
            self::$connect = new PDO("mysql:host=".self::$_hostname.";dbname=".self::$_dbName,self::$_username,"");
            self::$connect-> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$connect;
    }
    function __construct(){
        $this->_connectPDO=$this->getInstance();
    }
    function ExcuteQuery($sql){
		?>
		<!-- <?php //echo $sql ?>-->
		<?php
        $mang = array( );
        try{
            $this->_connectPDO->query("SET NAMES 'UTF8'");
            $this->stmt=$this->_connectPDO->query($sql);
            $mang=$this->stmt->fetchAll(PDO::FETCH_OBJ);
        }
        catch(Exception $e){
            echo $e->getMessage();
        }
        return $mang;
    }
    public function ExcuteObjectList($sql){
        try{
            $data = $this->ExcuteQuery($sql);
            return $data;
        }
        catch(Exception $e){
            throw new  $e->getMessage();
        }
        return null;
        /*$this->_adapter=mysql_query($sql);
        var_dump($this->_adapter);
        //var_dump(mysql_fetch_assoc($this->_adapter));
        //echo "<br/>";
        //var_dump(mysql_fetch_field($this->_adapter));
        //echo "<br/>";
        $ret=array();
        while($row=mysql_fetch_object($this->_adapter)){
                $ret[]=$row;
        }
        //var_dump($ret);
        //$string="qty";
        ///echo $ret[0]->$string;
        //die();
        return $ret;*/
    }
    public function ExcuteNonQuery($sql){
        try{
            $this->_connectPDO->query("SET NAMES 'UTF8'");
            $this->_connectPDO->exec($sql);
            return $this->_connectPDO->lastInsertId();
        }catch(Exception $e){
            echo $e->getMessage();
            throw new Exception;
        }
    }
    public function ExcuteNonQueryUpdate($sql){
        try{
            $this->_connectPDO->query("SET NAMES 'UTF8'");
            $this->_connectPDO->exec($sql);
            return "1";
        }catch(Exception $e){
            echo $e->getMessage();
            throw new Exception;
        }
    }
    public function ExcuteNonQueryDelete($sql){
        try{
            $this->_connectPDO->exec($sql);
            return "1";
        }catch(Exception $e){
            throw new $e->getMessage();
        }
    }

}