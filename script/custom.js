(function($) {

	$(document).ready(function() {
		$('#search-button').on('click', function(){
			$('header .search').fadeIn();
		});
		$('#close_search').on('click', function(){
			$('header .search').fadeOut();
		});
		$('#menu_button_tabblet, #menu-button').on('click', function(){
			$('.mega-menu').toggleClass('open');
			$('.sub-menu').fadeOut();
			if ($('.mega-menu > ul > li').hasClass('static')) {
				$('.mega-menu > ul > li').removeClass('static');
				$('.sub-menu').fadeOut();
			}
		});
		$('.click-sub-menu').on('click', function(){
			$('.mega-menu').toggleClass('open');
			$(this).siblings('.sub-menu').css('width', $('body').width());
			$(this).siblings('.sub-menu').fadeIn();
			$('.mega-menu > ul > li').addClass('static');
		});
		$('.click-sub-sub-menu').on('click', function() {
			$(this).parent().fadeOut(0);
			$('.mega-menu > ul > li').removeClass('static');
		});
		$('#login-button, #login_button_tabblet, .p-text').on('click', function(){
			$('#login_out').fadeIn();
		});
		$('#close_login_out').on('click', function(){
			$('#login_out').fadeOut(0);
		});
		$('.p-text, #close_shout_box').on('click', function(){
			$('.shout_box').fadeOut();
		});
		$('#chat_button').on('click', function(){
			$('.shout_box').fadeIn();
		});
		$('#online-button, #online_button').on('click', function(){
			$('.history').slideToggle();
		});
		//mouseup
		$(document).mouseup(function (e) {

			if (!$('#menu_button_tabblet, #menu-button').is(e.target) && !$('.mega-menu').is(e.target) && $('.mega-menu').has(e.target).length === 0) {
				$('.mega-menu').removeClass('open');
				if ( $('.mega-menu > ul > li').hasClass('static') ) {
					$('.sub-menu').fadeOut();				
				}
			}

			if (!$('#online-button, #online_button').is(e.target) && !$('.history').is(e.target) && $('.history').has(e.target).length === 0) {
				$('.history').fadeOut();
			}

		});
	});

	$(window).resize(function(){
	});
	
	
})(jQuery);