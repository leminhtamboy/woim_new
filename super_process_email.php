<?php
class Super_Process_Email{
    function __construct($tableName,$primaryKey){

    }
    function sendEmail($sender,$reciever,$html,$title){
        try {
            $to = $reciever;
            $subject = $title;
            $header=$this->getHeaderEmail(Super_Core::getHeaderEmail());
            $footer=$this->getFooterEmail(Super_Core::getFooterEmail());
            $message="";
            $message.=$header;
            $message.=$html;
            $message.=$footer;
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            // More headers
            $headers .= "From: <$sender>" . "\r\n";
            $headers .= "Cc: $sender" . "\r\n";
            mail($to, $subject, $message, $headers);
        }
        catch (Exception $err){
            throw  new Exception;
        }
    }
    function getHeaderEmail($pathOfHeader){
        $header=file_get_contents($pathOfHeader);
        return $header;
    }
    function getFooterEmail($pathOfFooter){
        $footer=file_get_contents($pathOfFooter);
        return $footer;
    }
}