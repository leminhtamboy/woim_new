<?php
include_once("Collection.php");
class Super_Process_Chat extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
    public function insertChat($user,$message,$date,$ip,$id_user){
            $this->setData("id", "NULL");
            $this->setData("user", $user);
            $this->setData("message", $message);
            $this->setData("date_time", $date);
            $this->setData("ip_address", $ip);
            $this->setData("id_user", $id_user);
            if ($this->inserRow() > 0) {
                $sql = "DELETE FROM shout_box WHERE id NOT IN (SELECT * FROM (SELECT id FROM shout_box ORDER BY id DESC LIMIT 0, 10) as sb)";
                $this->deleteBySqlCustom($sql);
            } else {

            }
        
    }
    public function showMessage(){
        $sqlGetMessage="SELECT user, message,id_user,date_time FROM (select * from shout_box ORDER BY id DESC LIMIT 10) shout_box ORDER BY shout_box.id ASC";
        $data=$this->getCollectionBySql($sqlGetMessage);
        foreach($data as $mes){
            $msg_time = date('h:i A M d',strtotime($mes->getdate_time())); //message posted time
            echo '<div class="shout_msg"><time>'.$msg_time.'</time><span class="username"><a href="/profile-user-'.$mes->getid_user().'.html" title="'.$mes->getuser().'">'.$mes->getuser().'</a></span> <span class="message">'.$mes->getmessage().'</span></div>';
        }
    }
}