<?php
$notice="";
?>
<style>
    .table tr td{
        margin-bottom: 10px;
    }
</style>

<script type="text/javascript">
    function checkForm(form) {
        if(!form.captcha.value.match(/^\d{5}$/)) {
        alert('Please enter the CAPTCHA digits in the box provided');
        form.captcha.focus();
        return false;
    }
        return true;
}

</script>
<?php
if(isset($_POST["actionregister"])){
    @session_start();
    if($_POST['captcha'] != $_SESSION['digit']){
        $notice.="Captcha không chính xác.Xin vui lòng nhập lại";
        @session_destroy();
    }else{
        $user=$superCore::getModel("users","id","Users");
        $username=$_POST["username"];
        $password=$_POST["password"];
        $email=$_POST["email"];
        $birthday=$_POST["birth_day"];
        $exsit=$user->CheckExsitEmail($email);
        if($exsit=="tontai"){
            $notice.="<br/>Email này đã tồn tại";
        }
        if($notice=="") {
            $kq = $user->RegisterAction($username, $password, $email, $birthday);
            if ($kq == "success") {
                ?>
                <script type="text/javascript">
                    alert("Đăng Kí Thành Công . Hệ thống sẽ tự động đăng nhập....");
                </script>
                <?php
                $dataUser = $user->LoginAction($email, $password);
                $dataUserRecord = $dataUser[0];
                $superCore->setSession($dataUserRecord, "user_id");
                ?>
                <script type="text/javascript">
                    top.location.href = "/";
                </script>
                <?php
            } else {
                $notice = "Đăng kí không thành công.";
            }
        }
    }

}
?>
<div class="clear category-data" style="box-sizing: border-box">
    <div class="title-of-block" style="border-top:1px rgb(255,255,255) solid; ">
        <h1>Đăng kí thành viên Woim</h1>
    </div>
    <p class="error"><?php echo $notice; ?></p>
    <div class="form-register">
        <form id="register" name="register" method="post" action="" onsubmit="return checkForm(this);">
            <table>
                <tr>
                    <td>
                        Tên Đăng Nhập<span class="red-alert">(*)</span>
                    </td>
                    <td>
                        <input type="text" name="username" value="" required/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Mật Khẩu<span class="red-alert">(*)</span>
                    </td>
                    <td>
                        <input type="password" name="password" value="" required/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Email<span class="red-alert">(*)</span>
                    </td>
                    <td>
                        <input type="text" name="email" value="" required/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Ngày Sinh (dd/mm/yyyy)
                    </td>
                    <td>
                        <input type="text" name="birth_day" value="">
                    </td>
                </tr>
            </table>
        <p align="center"><img src="/templates/capcha.php" width="120" height="30" border="1" alt="CAPTCHA"></p>
        <p align="center"><input type="text" size="6" maxlength="5" name="captcha" value=""><br>
            <small>copy the digits from the image into this box</small></p>
        <p style="padding-top: 10px">(*) là bắt buộc nhập </p>
        <input type="submit" value="Đăng Kí" class="button-login" style="margin-bottom:30px" name="actionregister">
        </form>
    </div>
</div>
