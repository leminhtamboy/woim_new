<?php
$listCategory=$detailSong->getSongInCategory($currentSong->getcategory_id());
?>
<?php if(count($listCategory) > 0  ){ ?>
    <div class="clear section po-r" style="margin-top:4%;">
        <h2 class="title-section">
            Có thể bạn thích nghe
        </h2>
        <div class="button-next-pre">
            <a class="home-page-next-combined" href="javascript:void(0);" style="float: right;"></a>
            <a class="home-page-prev-combined" href="javascript:void(0);" style="float: right;margin-right: 1%"></a>
        </div>
        <div class="clear"></div>
        <div class="home-page-slider">
            <ul id="you-also-like-per-song" style="list-style: none;">
                <?php foreach ($listCategory as $_album){?>
                    <?php
                    $imgUrl=$superCore->getImgesUrl()."/default_album.png";
                    $urlSong=$_album->getname()."-".$_album->getid();
                    ?>
                    <li class="list-sliders-li" style="height:auto !important;border:0">
                        <a href="<?php echo $urlSong ?>.html" class="homepage-sliders-href" target="_blank" title="<?php echo $_album->getname(); ?>">
                            <img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" style="margin: auto" class="homepage-img-slider">
                            <h1 style="text-align: center;padding-left: 0"><?php echo $_album->getname(); ?></h1>
                        </a>
                    </li>
                <?php } ?>
            </ul>
        </div>
        <script>
            jQuery(document).ready(function() {
                var list_album_perday= jQuery("#you-also-like-per-song");
                list_album_perday.owlCarousel({
                    pagination: false,
                    items : 4, //10 items above 1000px browser width
                    itemsDesktop : [1000,4], //5 items between 1000px and 901px
                    itemsDesktopSmall : [900,2], // betweem 900px and 601px
                    itemsTablet: [600,2], //2 items between 600 and 0
                    itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                    autoPlay:true,
                    autoPlayTimeout:3000,
                    autoPlayHoverPause:true,
                    autoPlaySpeed:3000
                });
                // Custom Navigation Events
                jQuery(".home-page-next-combined").click(function(){
                    var owl_next=jQuery("#you-also-like-per-song").data('owlCarousel');
                    owl_next.next();
                    return false;
                });
                jQuery(".home-page-prev-combined").click(function(){
                    var owl_next=jQuery("#you-also-like-per-song").data('owlCarousel');
                    owl_next.prev();;
                    return false
                });

            });
        </script>
    </div>
<?php } ?>