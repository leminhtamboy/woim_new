<?php
$listAlbum =$tag->getAlbumInTag($id_tag);
$listSong=$tag->getSongInTag($id_tag);
$path=$superCore->getWebUrl();
$rootWeb=$superCore->getRootWeb();
$title=$currentTag[0]->getname();
$seoLink=$superCore->toAscii($currentTag[0]->getname_ascii());
$des="";
if($des==""){
    $metaDescription="Thể loại Nhạc $title  Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
}
$metaKeyword=$title."nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
?>
<div class="title-of-block detail-category">
    <h1><a href="/nhac-cu-album-<?php echo $seoLink  ?>-<?php echo $id_tag ?>.html" title="<?php echo $title  ?>">Danh Sách Album thuộc nhạc cụ <?php echo $currentTag[0]->getname();  ?></a></h1>
</div>
<div class="category-data">
    <ul id="category-suggested-album" style="list-style: none">
        <?php foreach ($listAlbum as $_album){?>
            <?php
            $imgUrl=$rootWeb."/data/".$_album->getcover();
            if(!$superCore->checkIssetImage($imgUrl)){
                $imgUrl=$superCore->getImgesUrl()."/default_album.png";
            }
            ?>
            <li class="category-list-sliders-li">
                <a href="/album-<?php echo $superCore->toAscii($_album->getname_ascii()); ?>-<?php echo $_album->getid(); ?>.html" title="<?php echo $_album->getname(); ?>" class="homepage-sliders-href href-detail-category">
                    <img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>"  alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider detail-category-img">
                    <h1><?php echo $superCore::subStringLimit(20,$_album->getname()); ?></h1>
                </a>
            </li>
        <?php } ?>
    </ul>
	<div class="div-button" style="margin-bottom:20px" align="center">
		<a href="/nhac-cu-album-<?php echo $seoLink  ?>-<?php echo $id_tag ?>.html" class="button-login" title="<?php echo $title  ?>"> Xem thêm album</a>
	</div>
</div>
<div class="clear"></div>
<div class="title-of-block detail-category" style="margin-top: 3%">
    <h1><a href="/nhac-cu-song-<?php echo $seoLink  ?>-<?php echo $id_tag ?>.html" title="<?php echo $title  ?>">Danh Sách bài hát thuộc nhạc cụ <?php echo $currentTag[0]->getname();  ?></a></h1>
</div>
<div class="category-data">
    <ul id="category-suggested-album" style="list-style: none">
        <?php foreach ($listSong as $_album){?>
            <?php
            $imgUrl=$superCore->getImgesUrl()."/default_album.png";
            $urlSong=$superCore->toAscii($_album->getname_ascii())."-".$_album->getid();
            ?>
            <li class="category-list-sliders-li">
                <a href="<?php echo $urlSong ?>.html" class="homepage-sliders-href href-detail-category" title="<?php echo $_album->getname(); ?>">
                    <img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider detail-category-img">
                    <h1><?php echo $superCore::subStringLimit(20,$_album->getname()); ?></h1>
                </a>
            </li>
        <?php } ?>
    </ul>
	<div class="div-button" style="margin-bottom:20px" align="center">
		<a href="/nhac-cu-song-<?php echo $seoLink  ?>-<?php echo $id_tag ?>.html" class="button-login" title="<?php echo $title  ?>"> Xem thêm album</a>
	</div>
</div>
<div class="clear"></div>