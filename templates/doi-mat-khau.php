<?php
$notice="";
?>
<style>
    .table tr td{
        margin-bottom: 10px;
    }
</style>

<script type="text/javascript">
    function checkForm(form) {
        if(!form.captcha.value.match(/^\d{5}$/)) {
            alert('Please enter the CAPTCHA digits in the box provided');
            form.captcha.focus();
            return false;
        }
        return true;
    }

</script>
<?php
if(isset($_POST["actionregister"])){
    @session_start();
    if($_POST['captcha'] != $_SESSION['digit']){
        $notice.="Captcha không chính xác.Xin vui lòng nhập lại";
        @session_destroy();
    }else{
        $user=$superCore::getModel("users","id","Users");
        $old_password=$_POST["old_password"];
        $new_password=$_POST["new_password"];
        $correctPass=$user->checkOldPassword($old_password,$user_id);
        if(count($correctPass) < 1){
            $notice="<br/>Mật khẩu cũ không đúng . Xin vui lòng kiểm tra email";
        }else{
            if($notice=="") {
                $kq = $user->changePasswordNow($new_password,$user_id);
                    ?>
                    <script type="text/javascript">
                        alert("Đổi mật khẩu thành công . Xin vui lòng kiểm tra email của bạn");
                    </script>
                    <?php
            }
        }
    }
}
?>
<div class="clear category-data" style="box-sizing: border-box">
    <div class="title-of-block" style="border-top:1px rgb(255,255,255) solid; ">
        <h1>Đổi mật khẩu</h1>
    </div>
<p class="error"><?php echo $notice; ?></p>
<div class="form-register">
    <form id="register" name="register" method="post" action="" onsubmit="return checkForm(this);">
        <table>
            <tr>
                <td>
                    Mật  Khẩu Cũ <span class="red-alert">(*)</span>
                </td>
                <td>
                    <input type="text" name="old_password" style="width:100%" value="" required/>
                </td>
            </tr>
            <tr>
                <td>
                    Mật Khẩu Mới <span class="red-alert">(*)</span>
                </td>
                <td>
                    <input type="text" name="new_password" style="width:100%" value="" required/>
                </td>
            </tr>
        </table>
        <p align="center"><img src="/templates/capcha.php" width="120" height="30" border="1" alt="CAPTCHA"></p>
        <p align="center"><input type="text" size="6" maxlength="5" name="captcha" value=""><br>
            <small>copy the digits from the image into this box</small></p>
        <p style="padding-top: 10px">(*) là bắt buộc nhập </p>
        <input type="submit" class="button-login" style="margin-bottom:30px" value="Đổi Mật Khẩu" name="actionregister">
    </form>
</div>
</div>
