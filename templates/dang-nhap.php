<div class="block_right" id="login_out">
<?php
	if($superCore->checkIssetSession("user_id")){
?>
	<div class="content profile">
		<h2 class="title-login" style="text-align: center"><strong><?php echo $user->getname(); ?></strong></h2>
		<div class="center-img">
			<img class="profile_img" src="<?php echo $imgURL ?>" width="150" height="150" style="margin: auto"/>
		</div>
		<div class="profile_des" align="left">
			<ul>
				<li><a href="/profile-user-<?php echo $user->getid(); ?>.html">Xem Thông Tin Tài Khoản</a></li>
				<!--<li><a href="#">Tin Nhắn</a></li>
				<li><a href="#">Thông Báo</a></li>-->
				<li><a href="/user-album-like.html">Album và bài hát yêu thích</a></li>
				<li><a href="/doi-mat-khau.html">Đổi mật khẩu</a></li>
			</ul>
		</div>
	</div>
	<div class="clear"></div>
	<br/>
	<form action="logout.html" method="post" name="logoutForm">
		<div class="div-button" align="center">
			<input type="submit" class="button-login" name="logout" value="Đăng Xuất" />
			<input type="hidden" value="<?php echo (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" name="hidden_url" />
		</div>
	</form>
	<br/>
<?php
}else{
?>
<div class="title-login">Đăng nhập</div>
<div class="content">
<form action="" method="post" id="loginform">

<table width="100%" border="0">
<tbody>
	<tr>
		<td>
	<label id="user_name" for="username">Email</label>
	</td>
		<td>
	<input type="text" class="input-type" name="username" value="" maxlength="100" size="18" tabindex="99">
	</td>
	</tr>
	<tr style="padding-top: 25px">
		<td>
	<label id="pass_word" for="password">Mật khẩu</label>
	</td><td>
	<input type="password" class="input-type" name="password" value="" maxlength="100" size="18" tabindex="100">
	</td></tr>
</tbody>
</table>
	<div class="div-button" align="center">
		<input type="submit" class="button-login" name="login" value="Đăng nhập">
	</div>
</form>
<p class="register-account">
	<a href="/dang-ki-thanh-vien.html">Chưa có tài khoản ? (Đăng kí ngay)</a>
</p>
	<div id="login_status" style="display:none;" class="ajax_loading"></div>
	<div id="login_error" style="display:none;"></div>
</div>
<?php } ?>
</div>