<?php
include_once($config::$rootWeb."/super_process_Homepage.php");
$processHomepage = new Super_Process_HomePage("album","id");
$listAlbumPerDay = $processHomepage->getListAlbumInPerDay();
$listAlbumHearMost=$processHomepage->getListAlbumHearMost();
$path=$superCore->getWebUrl();
$rootWeb=$superCore->getRootWeb();
$listMoreView=$processHomepage->getListAlbumMoreView();
$radio_online_slider_left=$processHomepage->getRadioOnline();
$nghe_toi_ke_nay=$processHomepage->getNgheToiKeNay();
$cafe_sang=$processHomepage->getCafeSang();
@session_start();
$_SESSION["dau"]=0;
$_SESSION["cuoi"]=10;
?>
<script type="text/javascript">
	var img_loadding = '<?php echo $superCore->getImgesUrl() . "/loading-animation.gif"; ?>';
	jQuery("#img-cover-replace").attr("src", img_loadding);
	function loadDingFirstSlider(){
		var url = "<?php echo $superCore->getAjaxUrl() ?>" + "" + "Ajax_Load_First_Slider.php";
		jQuery.ajax({
			type: 'post',
			url: url,
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			cache: false,
			asyncBoolean: false,
			complete: function () {
			},
			error: function (err) {

			},
			success: function (result) {
				jQuery(".slider").html(result);
			}
		});
	}
	function loadSliderSuggest(){
		var url = "<?php echo $superCore->getAjaxUrl() ?>" + "" + "Ajax_Load_Slider_Homepage.php";
		jQuery.ajax({
			type: 'post',
			url: url,
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			cache: false,
			asyncBoolean: false,
			complete: function () {
			},
			error: function (err) {

			},
			success: function (result) {
				jQuery("#secondSliders").html(result);
			}
		});
	}

	setTimeout(function () {
		loadDingFirstSlider();
	}, 4000);
	setTimeout(function () {
		loadSliderSuggest();
	},5000);
</script>
<div class="slider-music">
	<div class="slider">
		<img src="<?php echo $superCore->getImgesUrl() . "/loading-animation.gif"; ?>" id="loading-img" style="display: block;width: inherit;height: inherit;margin: auto;"/>
	</div>
	<div class="new-music">
		<div class="block-new-one">
			<?php foreach($nghe_toi_ke_nay as $radio){ ?>
				<?php
				$imgUrl=$rootWeb."/data/".$radio->getcover();
				if(!$superCore->checkIssetImage($imgUrl)){
					$imgUrl=$superCore->getImgesUrl()."/default_album.png";
				}
				$name=$radio->getname();
				?>
			<a href="/album-<?php echo $superCore->toAscii($radio->getname_ascii()); ?>-<?php echo $radio->getid(); ?>.html" alt="<?php echo $name ?>" title="<?php echo $name; ?>">
				<img src="<?php echo $imgUrl; ?>" alt="<?php echo $name ?>" title="<?php echo $name; ?>" />
			</a>
			<?php break; } ?>
		</div>
		<div class="block-new-two">
			<?php foreach($cafe_sang as $radio){ ?>
			<?php
			$imgUrl=$rootWeb."/data/".$radio->getcover();
			if(!$superCore->checkIssetImage($imgUrl)){
				$imgUrl=$superCore->getImgesUrl()."/default_album.png";
			}
			$name=$radio->getname();
			?>
			<a href="/album-<?php echo $superCore->toAscii($radio->getname_ascii()); ?>-<?php echo $radio->getid(); ?>.html" alt="<?php echo $name ?>" title="<?php echo $name; ?>">
				<img src="<?php echo $imgUrl; ?>" alt="<?php echo $name ?>" title="<?php echo $name; ?>" />
			</a>
			<?php break; } ?>
		</div>
	</div>
</div>
<div class="clear"></div>
<div class="home-page-slider" id="secondSliders">
	<div style="width: 100%;height: 254px;display: block;">
	<img src="<?php echo $superCore->getImgesUrl() . "/loading-animation.gif"; ?>" id="loading-img" style="margin: auto;width: 250px;display: block;height: 250px;"/>
	</div>
</div>
<div class="homepage-category">
	<div class="title-of-block">
		<h1>Tìm kiếm nhiều nhất</h1>
	</div>
	<img id="img-search-more" src="" title="" alt="">
	<div class="home-page-slider-search-more">
		<!--<a class="home-page-prev" href="#"></a>-->
				<?php if(count($listAlbumPerDay) < 1  ){ ?>
				<?php $listAlbumPerDay = $processHomepage->getListAlbumRandom(); ?>
				<?php } ?>
				<?php foreach ($listAlbumPerDay as $_album){?>
				<div class="list-sliders-li">
					<?php 
					$imgUrl=$rootWeb."/data/".$_album->getcover();
					if(!$superCore->checkIssetImage($imgUrl)){
						$imgUrl=$superCore->getImgesUrl()."/default_album.png";
					}
					?>
					<a href="/album-<?php echo $superCore->toAscii($_album->getname_ascii()); ?>-<?php echo $_album->getid(); ?>.html" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" onmouseover="loadImageWhenHover(this)" class="homepage-sliders-href-search-new" data-img="<?php echo $imgUrl; ?>" data-name="<?php echo $_album->getname(); ?>">
						<!--<img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider">-->
						<h1><?php echo $_album->getname(); ?></h1>
					</a>
				</div>
				<?php } ?>
				<?php $listAlbumPerDay = $listMoreView; ?>
				<?php foreach ($listAlbumPerDay as $_album){ break; ?>
				<li class="list-sliders-li">
					<?php
					$imgUrl=$rootWeb."/data/".$_album->getcover();
					if(!$superCore->checkIssetImage($imgUrl)){
						$imgUrl=$superCore->getImgesUrl()."/default_album.png";
					}
					?>
					<a href="/album-<?php echo $superCore->toAscii($_album->getname_ascii()); ?>-<?php echo $_album->getid(); ?>.html" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" onmouseover="loadImageWhenHover(this)" class="homepage-sliders-href-search-new" data-img="<?php echo $imgUrl; ?>" data-name="<?php echo $_album->getname(); ?>">
						<!--<img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider">-->
						<h1><?php echo $_album->getname(); ?></h1>
					</a>
				</li>	
				<?php } ?>
		<!--<a class="home-page-next"  href="#"></a>-->
	</div>
</div>
<div class="hot-on-week">
	<div class="title-of-block">
		<h1>HOT Trong Tuần</h1>
	</div>
	<div class="content-of-hot-week">
		<ul class="list-hot">
			<!--<a class="home-page-prev" href="#"></a>-->
			<?php $listAlbumPerDay = $processHomepage->getListAlbumInPerDayOnHot(); ?>
			<?php foreach ($listAlbumPerDay as $_album){?>
				<li class="list-sliders-li-hot">
					<?php
					$imgUrl=$rootWeb."/data/".$_album->getcover();
					if(!$superCore->checkIssetImage($imgUrl)){
						$imgUrl=$superCore->getImgesUrl()."/default_album.png";
					}
					?>
					<a href="/album-<?php echo $superCore->toAscii($_album->getname_ascii()); ?>-<?php echo $_album->getid(); ?>.html" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>">
						<img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider hot-img">
						<h1><?php echo $superCore::subStringLimit(10,$_album->getname()); ?></h1>
					</a>
				</li>
			<?php } ?>
			<?php $listAlbumPerDay = $processHomepage->getListAlbumRandomOnHot(); ?>
			<?php foreach ($listAlbumPerDay as $_album){?>
				<li class="list-sliders-li-hot">
					<?php
					$imgUrl=$rootWeb."/data/".$_album->getcover();
					if(!$superCore->checkIssetImage($imgUrl)){
						$imgUrl=$superCore->getImgesUrl()."/default_album.png";
					}
					?>
					<a href="/album-<?php echo $superCore->toAscii($_album->getname_ascii()); ?>-<?php echo $_album->getid(); ?>.html" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>">
						<img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider hot-img">
						<h1><?php echo $superCore::subStringLimit(10,$_album->getname()); ?></h1>
					</a>
				</li>
			<?php } ?>
		</ul>
	</div>
</div>
<script>
	function loadImageWhenHover(elem){
		jQuery("#img-search-more").attr("src",jQuery(elem).attr("data-img"));
		jQuery("#img-search-more").attr("alt",jQuery(elem).attr("data-name"));
		jQuery("#img-search-more").attr("title",jQuery(elem).attr("data-name"));
	}
	function loadImageWhenHoverLoadFirst() {
		var elem=jQuery(".homepage-sliders-href-search-new").first();
		jQuery("#img-search-more").attr("src",elem.attr("data-img"));
		jQuery("#img-search-more").attr("alt",elem.attr("data-name"));
		jQuery("#img-search-more").attr("title",elem.attr("data-name"));
	}
	jQuery(document).ready(function() {
		loadImageWhenHoverLoadFirst();
		// Custom Navigation Events
		jQuery(".home-page-next").click(function(){
			var owl_next=jQuery("#homepage-suggested-album").data('owlCarousel');
			owl_next.next();
			return false;
		});
		jQuery(".home-page-prev").click(function(){
			var owl_next=jQuery("#homepage-suggested-album").data('owlCarousel');
			owl_next.prev();;
			return false
		});
		var list_album_perday= jQuery("#homepage-per-album");

});
</script>