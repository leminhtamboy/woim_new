<?php
if($isLogin==false){
    ?>
    <script>
        alert("Invalid token . Please login first");
        top.location.href="/";
    </script>
<?php
}
$notice="";
?>
<style>
    .table tr td{
        margin-bottom: 10px;
    }
</style>

<script type="text/javascript">
    function checkForm(form) {
        if(!form.captcha.value.match(/^\d{5}$/)) {
            alert('Please enter the CAPTCHA digits in the box provided');
            form.captcha.focus();
            return false;
        }
        return true;
    }

</script>
<?php
if(isset($_POST["actionregister"])){
    @session_start();
    if($_POST['captcha'] != $_SESSION['digit']){
        $notice.="Captcha không chính xác.Xin vui lòng nhập lại";
        @session_destroy();
    }else{
        $user=$superCore::getModel("users","id","Users");
        $email=$_POST["email"];
        $exsit=$user->CheckExsitEmail($email);
        if($exsit!="tontai"){
            $notice.="<br/>Email này không tồn tại.Xin vui lòng đăng kí";
        }
        if($notice=="") {
            $kq = $user->ChangePassWord($email);
            if ($kq != "") {
                $kq="Mật khẩu mới của bạn là :".$kq;
                $dataEmail=$superCore::getModel("email","alias_id","Email");
                $dataEmail->sendEmail($superCore::getSenderEmail(),$email,$kq,"Đổi mật khẩu");
                ?>
                <script type="text/javascript">
                    alert("Đổi mật khẩu thành công . Xin vui lòng kiểm tra email của bạn");
                </script>
                <?php
            } else {
                $notice = "Đổi mật khẩu không thành công.";
            }
        }
    }

}
?>
<div class="clear category-data" style="box-sizing: border-box">
    <div class="title-of-block" style="border-top:1px rgb(255,255,255) solid; ">
        <h1>Quên mật khẩu</h1>
    </div>
<p class="error"><?php echo $notice; ?></p>
<div class="form-register">
    <form id="register" name="register" method="post" action="" onsubmit="return checkForm(this);">
        <table>
            <tr>
                <td>
                    Email<span class="red-alert">(*)</span>
                </td>
                <td>
                    <input type="text" name="email"  value="" required/>
                </td>
            </tr>
        </table>
        <p align="center"><img src="/templates/capcha.php" width="120" height="30" border="1" alt="CAPTCHA"></p>
        <p align="center"><input type="text" size="6" maxlength="5" name="captcha" value=""><br>
            <small>copy the digits from the image into this box</small></p>
        <p style="padding-top: 10px">(*) là bắt buộc nhập </p>
        <input type="submit" value="Đổi Mật Khẩu" class="button-login" style="margin-bottom:30px" name="actionregister">
    </form>
</div>
