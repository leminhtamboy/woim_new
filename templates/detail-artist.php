<?php
$currentArtist=$artist->getDetailArtist($id_artist);
$dataArtist=$currentArtist;
?>
<div class="full-banner">
    <div class="container"> <img src="<?php echo $superCore->getArtistBanner(); ?>" alt="<?php echo $dataArtist[0]->getname(); ?>">
        <div class="box-info-artist">
            <div class="info-artist fluid">
                <div class="inside"> <img height="150" src="<?php echo $superCore->getImgesUrl().'/default_profile.jpg'; ?>" alt="<?php echo $dataArtist[0]->getname(); ?>">
                    <div class="info-summary">
                        <h1><?php echo $dataArtist[0]->getname(); ?></h1>
                        <div class="zfb">
                            <!--<div class="box-fb-like">
                                <div class="fb-like fb_iframe_widget" data-href="http://mp3.zing.vn/nghe-si/Huynh-Tu" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=1603079626622346&amp;container_width=0&amp;href=http%3A%2F%2Fmp3.zing.vn%2Fnghe-si%2FHuynh-Tu&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=true&amp;show_faces=false"><span style="vertical-align: bottom; width: 104px; height: 20px;"><iframe name="f28a002daa77eb8" width="1000px" height="1000px" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" src="https://www.facebook.com/v2.7/plugins/like.php?action=like&amp;app_id=1603079626622346&amp;channel=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FfTmIQU3LxvB.js%3Fversion%3D42%23cb%3Df14c811d8bdd858%26domain%3Dmp3.zing.vn%26origin%3Dhttp%253A%252F%252Fmp3.zing.vn%252Ff3107f0ce61243%26relation%3Dparent.parent&amp;container_width=0&amp;href=http%3A%2F%2Fmp3.zing.vn%2Fnghe-si%2FHuynh-Tu&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;share=true&amp;show_faces=false" style="border: none; visibility: visible; width: 104px; height: 20px;" class=""></iframe></span></div>
                            </div>-->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="example">
    <?php $listAlbumPerDay = $artist->getAlbumByArtist($id_artist); ?>
    <?php $rootWeb=$superCore->getRootWeb(); ?>
    <?php if(count($listAlbumPerDay) > 0  ){ ?>
        <div class="clear category-data" style="margin-top:10px;box-sizing: border-box">
            <div class="title-of-block" style="border-top:1px rgb(255,255,255) solid; ">
                <h1><a href="artist-<?php echo $dataArtist[0]->getname() ?>-<?php echo $dataArtist[0]->getid() ?>.html#album" target="_blank" title="Xem album <?php echo $dataArtist[0]->getname() ?>">Album <?php echo $dataArtist[0]->getname() ?>  <i class="icon-arrow"></i></a></h1>
            </div>
            <div class="home-page-slider">
                <a class="home-page-prev" style="background-color: transparent" href="javascript:void(0);"></a>
                <ul id="homepage-per-album">
                    <?php foreach ($listAlbumPerDay as $_album){ ?>
                        <?php
                        $imgUrl=$rootWeb."/data/".$_album->getcover();
                        if(!$superCore->checkIssetImage($imgUrl)){
                            $imgUrl=$superCore->getImgesUrl()."/default_album.png";
                        }
                        ?>
                        <li class="list-sliders-li">
                            <a href="/album-<?php echo $superCore->toAscii($_album->getname_ascii()); ?>-<?php echo $_album->getid(); ?>.html" alt="<?php echo $_album->getname(); ?>" class="homepage-sliders-href" title="<?php echo $_album->getname(); ?>">
                                <img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider">
                                <h1><?php echo $superCore::subStringLimit(15,$_album->getname()); ?></h1>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                <a class="home-page-next" style="background-color: transparent"  href="javascript:void(0);"></a>
            </div>
            <script>
                jQuery(document).ready(function() {
                    var list_album_perday= jQuery("#homepage-per-album");
                    list_album_perday.owlCarousel({
                        pagination: false,
                        items : 4, //10 items above 1000px browser width
                        itemsDesktop : [1000,4], //5 items between 1000px and 901px
                        itemsDesktopSmall : [900,2], // betweem 900px and 601px
                        itemsTablet: [600,2], //2 items between 600 and 0
                        itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                        autoPlay:true,
                        autoPlayTimeout:3000,
                        autoPlayHoverPause:true,
                        autoPlaySpeed:3000
                    });
                    // Custom Navigation Events
                    jQuery(".home-page-next").click(function(){
                        var owl_next=jQuery("#homepage-per-album").data('owlCarousel');
                        owl_next.next();
                        return false;
                    });
                    jQuery(".home-page-prev").click(function(){
                        var owl_next=jQuery("#homepage-per-album").data('owlCarousel');
                        owl_next.prev();;
                        return false
                    });

                });
            </script>
        </div>
    <?php } ?>
    <p></p>
    <?php $listSong = $artist->getSongByArtist($id_artist); ?>
    <?php if(count($listSong) > 0  ){ ?>
        <div class="clear category-data">
            <div class="title-of-block detail-category">
                <h1><a href="artist-<?php echo $dataArtist[0]->getname() ?>-<?php echo $dataArtist[0]->getid() ?>.html#song" target="_blank" title="Xem album <?php echo $dataArtist[0]->getname() ?>">Nhạc <?php echo $dataArtist[0]->getname() ?>  <i class="icon-arrow"></i></a></h1>
            </div>
            <div class="home-page-slider">
                <a class="home-page-prev" style="background-color: transparent" href="javascript:void(0);"></a>
                <ul id="homepage-per-song" style="list-style: none;">
                    <?php foreach ($listSong as $_album){?>
                        <?php
                        $imgUrl=$superCore->getImgesUrl()."/default_album.png";
                        $urlSong=$superCore->toAscii($_album->getname())."-".$_album->getid();
                        ?>
                        <li class="list-sliders-li">
                            <a href="<?php echo $urlSong ?>.html" class="homepage-sliders-href" target="_blank" title="<?php echo $_album->getname(); ?>">
                                <img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider">
                                <h1><?php echo $superCore::subStringLimit(15,$_album->getname()); ?></h1>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
                <a class="home-page-next" style="background-color: transparent"  href="javascript:void(0);"></a>
            </div>
            <script>
                jQuery(document).ready(function() {
                    var list_album_perday= jQuery("#homepage-per-song");
                    list_album_perday.owlCarousel({
                        pagination: false,
                        items : 4, //10 items above 1000px browser width
                        itemsDesktop : [1000,4], //5 items between 1000px and 901px
                        itemsDesktopSmall : [900,2], // betweem 900px and 601px
                        itemsTablet: [600,2], //2 items between 600 and 0
                        itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                        autoPlay:true,
                        autoPlayTimeout:3000,
                        autoPlayHoverPause:true,
                        autoPlaySpeed:3000
                    });
                    // Custom Navigation Events
                    jQuery(".home-page-next").click(function(){
                        var owl_next=jQuery("#homepage-per-song").data('owlCarousel');
                        owl_next.next();
                        return false;
                    });
                    jQuery(".home-page-prev").click(function(){
                        var owl_next=jQuery("#homepage-per-song").data('owlCarousel');
                        owl_next.prev();;
                        return false
                    });

                });
            </script>
        </div>
    <?php } ?>
</div>


