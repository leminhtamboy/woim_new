<?php
if(!isset($_REQUEST["q"])){
    ?>
    <script>
        top.location.href="/";
    </script>
<?php
}
$path=$superCore->getWebUrl();
$rootWeb=$superCore->getRootWeb();
$title=$keywordSearch;
$des="";
if($des==""){
    $metaDescription="Thể loại Nhạc $title  Không Lời - Nhạc Hòa Tấu - Nhạc Cổ Điển - Radio Online. World of Instrumental Music - Nơi chia sẽ mọi cảm xúc âm nhạc không lời lớn nhất Việt Nam";
}
$metaKeyword=$title."nhạc không lời, nhạc hòa tấu, nhac không loi, nhac hoa tau, newage music, nhạc cổ điển, audiophile, instrumental music, nhac phap hay nhat, download nhac, hoatau, nhac nen khong loi, nhac hoa tau guitar";
$so_luong_tim=count($listAlbum);
?>
<div class="title-of-block detail-category">
    <h1><a href="/tim-kiem-album-<?php echo $seoLink  ?>-<?php echo $id_category ?>.html" title="<?php echo $title  ?>">Tìm kiếm - <?php echo $titleSearch; ?> <?php  echo $keywordSearch; ?> có (<?php echo number_format($so_luong_tim) ?>) kết quả</a></h1>
</div>
<?php if($so_luong_tim > 0){ ?>
<div class="category-search">
    <ul id="category-suggested-album" class="ul-search" style="list-style: none">
        <?php foreach ($listAlbum as $_album){?>
            <?php
            $imgUrl=$rootWeb."/data/".$_album->getcover();
            if(!$superCore->checkIssetImage($imgUrl)){
                $imgUrl=$superCore->getImgesUrl()."/default_album.png";
            }
            $luot_nghe=$_album->getalbum_view();
            $ten_tac_gia=$_album->getname_artist();
            $ten_album=$_album->getname_album();
            $id=$_album->getid_album();
            $ascii=$_album->getname_ascii_album();
            $id_artist=$_album->getid_artist();
            $name_seo=$_album->getname_ascii_artist();
            ?>
            <li class="search-li">
                <a href="/<?php echo $Link; ?><?php echo $superCore->toAscii($ascii); ?>-<?php echo $id; ?>.html" title="<?php echo $ten_album; ?>" class="href-detail-category">
                    <img src="<?php echo $imgUrl; ?>" alt="<?php echo $ten_album; ?>" title="<?php echo $ten_album; ?>" class="img-search">
                    <div class="detal-search-information">
                        <h1><?php echo Super_Core::Detected_Search($keywordSearch,$ten_album); ?> - <a href="artist-<?php echo $superCore->toAscii($name_seo); ?>-<?php echo $id_artist; ?>.html"><?php echo Super_Core::Detected_Search($keywordSearch,$ten_tac_gia); ?></a></h1>
                        <div class="infor-meta">
                            <span>Lượt nghe : </span>
                            <span class="number-listen"><?php echo $luot_nghe ?></span>
                        </div>
                    </div>
                </a>
            </li>
            <div class="clear"></div>
        <?php } ?>
    </ul>
</div>
<div class="clear"></div>
<ul style="display: block;font-weight: bold;font-size: 15px;" align="center">
    <li class="li-page">
        <a <?php if($page==0) echo $classActive; ?> href="<?php  echo $urlCurrent?>-0.html"> << </a>
    </li>
    <li class="li-page">
        <a href="<?php  echo $urlCurrent?>-<?php echo $pre; ?>.html"> < </a>
    </li>
    <?php
    for($p=$beforeTra;$p<$endTra;$p++){
        ?>
        <li class="li-page <?php if($page==$p) echo $classActive; ?>">
            <a href="<?php  echo $urlCurrent?>-<?php echo $p ?>.html"> <?php echo $p+1 ?> </a>
        </li>
        <?php
    }
    ?>
    <li class="li-page">
        <a href="<?php  echo $urlCurrent?>-<?php echo $next  ; ?>.html"> > </a>
    </li>
    <li class="li-page">
        <a href="<?php  echo $urlCurrent?>-<?php echo $endPage-1 ?>.html"> >> </a>
    </li>
</ul>
<?php }else{ ?>
    <p class="non-search">Bạn tìm ko thấy? Hãy gửi yêu cầu để woim cập nhật nhé.</p>
<?php } ?>
<div class="clear"></div>