<?php
$path=$superCore->getWebUrl();
$rootWeb=$superCore->getRootWeb();
?>
<div class="title-of-block detail-category">
    <h1><a href="/the-loai-album-<?php echo $title  ?>-<?php echo $id_category ?>.html" title="<?php echo $title  ?>">Danh Sách bài hát thuộc <?php echo $title  ?></a></h1>
</div>
<div class="category-data">
    <ul id="category-suggested-album" style="list-style: none">
        <?php foreach ($listSong as $_album){?>
            <?php
            $imgUrl=$superCore->getImgesUrl()."/default_album.png";
            $urlSong=$_album->getname()."-".$_album->getid();
            ?>
            <li class="category-list-sliders-li">
                <a href="/album-<?php echo $superCore->toAscii($_album->getname_ascii()); ?>-<?php echo $_album->getid(); ?>.html" title="<?php echo $_album->getname(); ?>" class="homepage-sliders-href href-detail-category">
                    <img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" class="homepage-img-slider detail-category-img">
                    <h1><?php echo $superCore::subStringLimit(20,$_album->getname()); ?></h1>
                </a>
            </li>
        <?php } ?>
    </ul>
</div>
<div class="clear"></div>
<ul style="display: block;font-weight: bold;font-size: 15px;" align="center">
    <li class="li-page">
        <a <?php if($page==0) echo $classActive; ?> href="<?php  echo $urlCurrent?>-0.html"> << </a>
    </li>
    <li class="li-page">
        <a href="<?php  echo $urlCurrent?>-<?php echo $pre; ?>.html"> < </a>
    </li>
    <?php
    for($p=$beforeTra;$p<$endTra;$p++){
        ?>
        <li class="li-page <?php if($page==$p) echo $classActive; ?>">
            <a href="<?php  echo $urlCurrent?>-<?php echo $p ?>.html"> <?php echo $p+1 ?> </a>
        </li>
        <?php
    }
    ?>
    <li class="li-page">
        <a href="<?php  echo $urlCurrent?>-<?php echo $next  ; ?>.html"> > </a>
    </li>
    <li class="li-page">
        <a href="<?php  echo $urlCurrent?>-<?php echo $endPage-1 ?>.html"> >> </a>
    </li>
</ul>