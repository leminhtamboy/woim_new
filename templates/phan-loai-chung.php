<?php
/**
 * Created by Lê Minh Tâm
 * Lấy danh sách recentview
 * Lấy danh sach click vào favorite
 * Lấy danh sách của hiện tại là dựa theo 3 tiêu chí category id , album id and artist id
 */
?>
<?php
$classRecent=Super_Core::getModel("recent","id_recent","Recent");
$listRecent=$classRecent->getListRecent($user_id);
$rootWeb=$superCore->getRootWeb();
?>
    <div class="clear section po-r" style="margin-top:10px;">
        <h2 class="title-section">
            Có thể bạn thích nghe
        </h2>
        <div class="button-next-pre">
            <a class="home-page-next-combined" href="javascript:void(0);" style="float: right;"></a>
            <a class="home-page-prev-combined" href="javascript:void(0);" style="float: right;margin-right: 1%"></a>
        </div>
        <div class="clear"></div>
        <div class="home-page-slider">
            <ul id="you-also-like-per-song" style="list-style: none;">
                <?php if(count($listRecent) > 0){ ?>
                    <?php $arrayIdSong=""; ?>
                    <?php foreach($listRecent as $_recent){ ?>
                        <?php if($_recent->getalbum_id()!=0){ ?>
                            <?php $arrayIdSong.=$_recent->getalbum_id().","; ?>
                        <?php } ?>
                    <?php } ?>
                    <?php $arrayIdSong=$superCore->processStringForInQuery($arrayIdSong); ?>
                    <?php $listSongRecent=$classRecent->getSongRecent($arrayIdSong); ?>
                    <?php foreach ($listSongRecent as $_album){ ?>
                        <?php
                        $imgUrl=$rootWeb."/data/".$_album->getcover();
                        if(!$superCore->checkIssetImage($imgUrl)){
                            $imgUrl=$superCore->getImgesUrl()."/default_album.png";
                        }
                        $urlSong="/album-".$superCore->toAscii($_album->getname_ascii())."-".$_album->getid();
                        ?>
                        <li class="list-sliders-li" style="height:auto !important;border:0">
                            <a href="<?php echo $urlSong ?>.html" class="homepage-sliders-href" title="<?php echo $_album->getname(); ?>">
                                <img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" style="margin: auto" class="homepage-img-slider">
                                <h1 style="text-align: center;padding-left: 0"><?php echo $_album->getname(); ?></h1>
                            </a>
                        </li>
                    <?php } ?>
                <?php }else{ ?>
                <?php
                    include_once ("../super_process_Homepage.php");
                    $class_home_page= new Super_Process_HomePage("album","id");
                ?>
                    <?php $dataRandomAlbum=$class_home_page->getListAlbumMostView() ?>
                    <?php foreach ($dataRandomAlbum as $_album){ ?>
                        <?php
                        $imgUrl=$rootWeb."/data/".$_album->getcover();
                        if(!$superCore->checkIssetImage($imgUrl)){
                            $imgUrl=$superCore->getImgesUrl()."/default_album.png";
                        }
                        $urlSong="/album-".$superCore->toAscii($_album->getname_ascii())."-".$_album->getid();
                        ?>
                        <li class="list-sliders-li" style="height:auto !important;border:0">
                            <a href="<?php echo $urlSong ?>.html" class="homepage-sliders-href" title="<?php echo $_album->getname(); ?>">
                                <img src="<?php echo $imgUrl; ?>" alt="<?php echo $_album->getname(); ?>" title="<?php echo $_album->getname(); ?>" style="margin: auto" class="homepage-img-slider">
                                <h1 style="text-align: center;padding-left: 0"><?php echo $_album->getname(); ?></h1>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
        <script>
            jQuery(document).ready(function() {
                var list_album_perday= jQuery("#you-also-like-per-song");
                list_album_perday.owlCarousel({
                    pagination: false,
                    items : 6, //10 items above 1000px browser width
                    itemsDesktop : [1030,4], //5 items between 1000px and 901px
                    itemsDesktopSmall : [900,2], // betweem 900px and 601px
                    itemsTablet: [480,1], //2 items between 600 and 0
                    itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
                    autoPlay:true,
                    autoPlayTimeout:3000,
                    autoPlayHoverPause:true,
                    autoPlaySpeed:3000
                });
                // Custom Navigation Events
                jQuery(".home-page-next-combined").click(function(){
                    var owl_next=jQuery("#you-also-like-per-song").data('owlCarousel');
                    owl_next.next();
                    return false;
                });
                jQuery(".home-page-prev-combined").click(function(){
                    var owl_next=jQuery("#you-also-like-per-song").data('owlCarousel');
                    owl_next.prev();;
                    return false
                });

            });
        </script>
    </div>