<?php
include_once("Collection.php");
define('SUPERAI_ROOT',$_SERVER['DOCUMENT_ROOT']);
class ConfigGlobal extends  Collection{

    public static $rootWeb=SUPERAI_ROOT;
    //public static $realPath="http://woim.me/";
    public static $realPath="http://www.dev.woim.me/";
    //public static $realPath="http://www.dev.woim.com/";
    function __construct($tableName=null, $primaryKey=null)
    {
        parent::__construct($tableName, $primaryKey);
    }
    function loadbyPath($configName){
        $data=$this->loadByAttribute("config_name","eq",$configName,1);
        return $data->getvalue();
    }
}