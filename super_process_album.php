<?php
include_once("Collection.php");
class Super_Process_Album extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
    function getListAlbumBelongToCategory($id_category,$start,$end){
        if($start==0  && $end ==0){
            $sql = "select * from album where category_id=$id_category";
        }else {
            $sql = "select * from album where category_id=$id_category limit $start,$end";
        }
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getListAlbumBelongToTag($id_category,$start,$end){
        if($start==0  && $end ==0){
            $sql = "select * from album where tag_id=$id_category";
        }else {
            $sql = "select * from album where tag_id=$id_category limit $start,$end";
        }
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getListSongBelongToCategory($id_category,$start,$end){
        if($start==0  && $end ==0){
            $sql = "select * from song where category_id=$id_category";
        }else {
            $sql = "select * from song where category_id=$id_category limit $start,$end";
        }
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getListSongBelongToTag($id_category,$start,$end){
        if($start==0  && $end ==0){
            $sql = "select * from song where tag_id=$id_category";
        }else {
            $sql = "select * from song where tag_id=$id_category limit $start,$end";
        }
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getListSongOfAlbum($idAlbum){
        $sql="select * from song where album_id=$idAlbum";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getListAlbumInPerDay(){
        $currentDay=date("Y-m-d");
        $sql="select * from album where date_modifier like '$currentDay' limit 0 ,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getListAlbumRandom(){
        $currentDay=date("Y-m-d");
        $sql="select * from album order by rand() limit 0 ,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getAlbumByIds($arrayIds){
        $sql="select * from album where id in ($arrayIds)";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getSongByIds($arrayIds){
        $sql="select * from song where id in ($arrayIds)";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getListSontByTag($id_tag){
        $sql="select *,album.id as id_album,album.name as name_album,category.name as name_category,artist.name as name_artist from album 
		left join artist on album.artist_id=artist.id 
		left join category on album.category_id=category.id
		where album.tag_id=$id_tag order by rand() limit 0 , 5 ";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getListTag($arrayTags){
        $sql="select * from tag where id in($arrayTags)";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function subStringLimit($limit,$string){
        return substr($string,0,$limit)."...";
    }
    function getArtist($id){
        $sql="select * from artist where id=$id";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getLikeAlbum($id_album){
        $sql="select id from like_album where album_id=$id_album";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function checkIsLikeAlbum($id_album,$user_id){
        $sql="select id from like_album where album_id=$id_album and user_id='$user_id'";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getCommentOnAlbum($id_album){
        $sql="select content,username from album_comment where album_id=$id_album order by id desc limit 0,15 ";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function addCommentOnAlbum($text,$user_id,$userName,$album_id){
        $albumData = new Collection("album_comment","id");
        $albumData->setData("id","NULL");
        $albumData->setData("album_id",$album_id);
        $albumData->setData("content",$text);
        $albumData->setData("user_id",$user_id);
        $albumData->setData("username",$userName);
        $albumData->inserRow();
    }
    function addCommentOnSong($text,$user_id,$userName,$song_id){
        $albumData = new Collection("song_comment","id");
        $albumData->setData("id","NULL");
        $albumData->setData("song_id",$song_id);
        $albumData->setData("content",$text);
        $albumData->setData("user_id",$user_id);
        $albumData->setData("username",$userName);
        $albumData->inserRow();
    }

}