<?php
include_once("Collection.php");
class Super_Process_Nhaccu extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
    function getDetailTag($id_tag){
        $sql="select * from tag where id=$id_tag";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getAlbumInTag($id_tag){
        $sql="select * from album where tag_id=$id_tag order by id desc limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getSongInTag($id_tag){
        $sql="select * from song where tag_id=$id_tag order by id desc limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
}