<?php
include_once("Row.php");
include_once("Super_Abstract.php");
class Collection extends Super_Abstract
{
    var $_keylist=array();
    var $_valuelist=array();
    var $_collection;
    var $row;
    function __construct($tableName,$primaryKey)
    {
        parent::__construct($tableName,$primaryKey);
    }
    public function load($id){
        $objects=parent::getLoadCollectionByPrimaryKey($id);
        $this->row = array();
        foreach($objects as $ob){
            $_row = new Row($ob);
            $this->row[]=$_row;
        }
        if($id!=-1){
            return $this->row[0];
        }
        return null;
    }
    public function loadByAttribute($columnName,$condition="eq",$value,$numRow=1){
        $objects=parent::getLoadCollectionByAttribute($columnName,$condition,$value);
        if(count($objects) > 0) {
            $this->row = array();
            foreach ($objects as $ob) {
                $_row = new Row($ob);
                $this->row[] = $_row;
            }
            if($numRow==1){
                if ($this->row[0]) {
                    return $this->row[0];
                }
            }else{
                return $this->row;
            }
        }else{
            return "none";
        }
        throw  new Exception;
    }
    public function loadByAttributeArray($arrayColumn,$arrayCondition,$arrayValue,$numRow){
        $objects=parent::getLoadCollectionByAttributeArray($arrayColumn,$arrayCondition,$arrayValue);
        if(count($objects) > 0) {
            $this->row = array();
            foreach ($objects as $ob) {
                $_row = new Row($ob);
                $this->row[] = $_row;
            }
            if($numRow==1){
                if ($this->row[0]) {
                    return $this->row[0];
                }
            }else{
                return $this->row;
            }
        }else{
            return "none";
        }
        throw  new Exception;
    }
    public function getCollection(){
        $objects=parent::getAbstractCollection();
        $this->row = array();
        foreach($objects as $ob){
            $_row = new Row($ob);
            $this->row[]=$_row;
        }
        return $this->row;
    }
    public function getCollectionBySql($sql){
        $objects=parent::getAbstractCustomCollection($sql);
        $this->row = array();
        foreach($objects as $ob){
            $_row = new Row($ob);
            $this->row[]=$_row;
        }
        return $this->row;
    }
    public function inserRow(){
        $numRows=parent::insertDB($this->_keylist,$this->_valuelist);
        //do something with numRows
        return $numRows;
    }
    public function updateRow(){
        //TO DO
        $numRows=parent::updateDB($this->_keylist,$this->_valuelist);
        return $numRows;
    }
    public function deleteRow($id){
        $numRows=parent::deleteDB($id);
    }
    public function deleteBySqlCustom($sql){
        $numRows=parent::deleteBySql($sql);
    }
    public function setData($nameColumn,$value){
        $this->_keylist[]=$nameColumn;
        $this->_valuelist[]=$value;
    }
    public function addAttributeToSelect($array){
        $countArray=count($array);
        $stringSelect="";
        if($countArray > 0){
            for($i=0;$i<$countArray;$i++){
                $stringSelect.=$array[$i].",";
            }
            $stringSelect=substr($stringSelect,0,strlen($stringSelect)-2);
        }else{
            $stringSelect=$array;
        }
    }
    public function addAttributeToFilter($stringFilter){
        $this->_sql.=" where ".$stringFilter;
    }
    public function join($stringJoin){

    }
    //public function getData($fieldName)
    //{
        //return $this->$fieldName;
    //}
}