<?php
include_once("Collection.php");
class Super_Process_Category extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
	function getListMenu(){
		$sql="select * from category where parent=0";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getChildrenMenu($parent_id){
		$sql="select * from category where parent=$parent_id";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getDetailCategory($id_category){
		$sql="select * from category where id=$id_category";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getAlbumInCategory($id){
		$sql="select * from album where category_id=$id order by id desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSongInCategory($id_category){
		$sql="select * from song where category_id=$id_category order by id desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getAlbumBySearch($key_search,$start,$limit){
		$sql="";
		if($start==0 && $limit==0){
			$sql="select id from album where name like '%$key_search%'";
		}else {
			$sql = "select *,album.name_ascii as name_ascii_album,album.id as id_album,album.name as name_album,album.view as album_view,artist.name as name_artist,artist.id as id_artist,artist.name_ascii as name_ascii_artist from album inner join artist on album.artist_id=artist.id where album.name like '%$key_search%' limit $start,$limit";
		}
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSongBySearch($key_search,$start,$limit){
		$sql="";
		if($start==0 && $limit==0){
			$sql="select id from song where name like '%$key_search%'";
		}else {
			$sql = "select *,song.name_ascii as name_ascii_album,song.id as id_album,song.name as name_album,song.view as album_view,artist.name as name_artist,artist.id as id_artist,artist.name_ascii as name_ascii_artist from song  inner join album on song.album_id=album.id inner join artist on album.artist_id=artist.id where song.name like '%$key_search%' limit $start,$limit";
		}
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getAlbumBySearchArtist($key_search,$start,$limit){
		$sql="";
		if($start==0 && $limit==0){
			$sql="select album.id from album inner join artist on album.artist_id=artist.id  where artist.name  like '%$key_search%'";
		}else {
			$sql = "select *,album.name_ascii as name_ascii_album,album.id as id_album,album.name as name_album,album.view as album_view,artist.name as name_artist,artist.id as id_artist,artist.name_ascii as name_ascii_artist from album inner join artist on album.artist_id=artist.id where artist.name like '%$key_search%' limit $start,$limit";
		}
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSongBySearchArtist($key_search){
		$sql="select * from song where name like '%$key_search%' limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getListAlbumBelongToCategory($key_search,$start,$end){
		if($start==0  && $end ==0){
			$sql="select * from album where name like '%$key_search%' limit 0,10";
		}else {
			$sql="select * from album where name like '%$key_search%' limit $start,$end";
		}
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getListSongBelongToCategory($key_search,$start,$end){
		if($start==0  && $end ==0){
			$sql="select * from song where name like '%$key_search%' limit 0,10";
		}else {
			$sql="select * from song where name like '%$key_search%' limit $start,$end";
		}
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
}