<?php
include_once("Collection.php");
class Super_Process_HomePage extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
	function getListAlbumMostView(){
		$sql="select * from album order by rand() desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getListAlbumMoreView(){
		$sql="select * from album order by album.view desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	public function getRadioOnline(){
		$sql="select * from album where category_id=14 order by id desc limit 0,5";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	public function getNgheToiKeNay(){
		$sql="select * from album where category_id=15 order by rand() desc limit 0,5";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	public function getCafeSang(){
		/*$sql="select * from album where category_id=16 order by id desc limit 0,5";*/
		$sql="select * from album where tag_id=1 order by rand() desc limit 0,5";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getListSongOfAlbum($idAlbum){
		$sql="select * from song where album_id=$idAlbum";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getListAlbumInPerDay(){
		$currentDay=date("Y-m-d");
		/*$sql="select * from album where date_modifier <= '$currentDay' limit 0 ,10";*/
		$sql="select * from album order by rand()  limit 0 ,10";
		$data=$this->getCollectionBySql($sql);
        return $data;	
	}
	function getListAlbumInPerDayOnHot(){
		$currentDay=date("Y-m-d");
		$sql="select * from album order by rand() desc limit 0 ,8";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getListAlbumRandomOnHot(){
		$sql="select * from album order by rand() desc limit 0,8";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getListAlbumHearMost(){
		$sql="select * from album order by view desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getAlbumRecent($user_id){
		$sqlGetAlbum="select * from recent where album_id != 0 and user_id='$user_id'";
		$dataAlbum=$this->getCollectionBySql($sqlGetAlbum);
		$idAlbum="";
		foreach($dataAlbum as $_recent){
			$idAlbum.=$_recent->getalbum_id().",";
		}
		$idAlbum=substr($idAlbum,0,strlen($idAlbum)-1);
		if($idAlbum==""){
			$idAlbum=rand (1,1000);
		}
		$sql="select * from album where id in ($idAlbum)";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getListAlbumRandom(){
		$currentDay=date("Y-m-d");
		$sql="select * from album order by id desc limit 0 ,10";
		$data=$this->getCollectionBySql($sql);
        return $data;	
	}
	
	function getAlbumByIds($arrayIds){
		$sql="select * from album where id in ($arrayIds)";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSongByIds($arrayIds){
		$sql="select * from song where id in ($arrayIds)";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getListSontByTag($id_tag){
		$sql="select *,album.id as id_album,album.name as name_album,category.name as name_category,artist.name as name_artist from album 
		left join artist on album.artist_id=artist.id 
		left join category on album.category_id=category.id
		where album.tag_id=$id_tag order by rand()   limit 0 , 5 ";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getListTag($arrayTags){
		$sql="select * from tag where id in($arrayTags)";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function subStringLimit($limit,$string){
		return substr($string,0,$limit)."...";
	}
	function getArtist($id){
		$sql="select * from artist where id=$id";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getLikeAlbum($id_album){
		$sql="select id from like_album where album_id=$id_album";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function checkIsLikeAlbum($id_album,$user_id){
		$sql="select id from like_album where album_id=$id_album and user_id='$user_id'";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getCommentOnAlbum($id_album){
		$sql="select content,username from album_comment where album_id=$id_album order by id desc limit 0,15 ";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function addCommentOnAlbum($text,$user_id,$userName,$album_id){
		$albumData = new Collection("album_comment","id");
		$albumData->setData("id","NULL");
		$albumData->setData("album_id",$album_id);
		$albumData->setData("content",$text);
		$albumData->setData("user_id",$user_id);
		$albumData->setData("username",$userName);
		$albumData->inserRow();
	}
	function addCommentOnSong($text,$user_id,$userName,$song_id){
		$albumData = new Collection("song_comment","id");
		$albumData->setData("id","NULL");
		$albumData->setData("song_id",$song_id);
		$albumData->setData("content",$text);
		$albumData->setData("user_id",$user_id);
		$albumData->setData("username",$userName);
		$albumData->inserRow();
	}
	function updateListen($id_album,$viewCount){
		$viewCount=$viewCount+1;
		$this->setData("id",$id_album);
		$this->setData("view",$viewCount);
		$this->updateRow();
	}

}