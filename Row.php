<?php
class Row {

    var $_stdClass=null;
    public function __construct($object)
    {
        $this->_stdClass=$object;
    }
    public function getData($fieldName){
        return $this->_stdClass->$fieldName;
    }
    public function __call($name, $arguments)
    {
        $realName=substr($name,3);
        return $this->getData($realName);
        // TODO: Implement __call() method.
    }
}