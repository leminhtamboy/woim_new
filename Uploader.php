<?php
include_once("../Config.php");
class UploadDer{
    public $_mainFoler="";
    public $_nameFile="";
    public $_nameTagFile="";
    public function uploadAction(){
        $baseDir=ConfigGlobal::$rootWeb."/image/".$this->_mainFoler;
        $path = $baseDir;
        @mkdir($path, 777, true);
        if (isset($_FILES[$this->_nameTagFile]['name']) && $_FILES[$this->_nameTagFile]['name']!="") {
            try {
                $path = $path ."/".$_FILES[$this->_nameTagFile]['name'];
                file_put_contents("$path",file_get_contents($_FILES[$this->_nameTagFile]['tmp_name']));
                $pathReturn=ConfigGlobal::$realPath."/image/".$this->_mainFoler."/".$_FILES[$this->_nameTagFile]['name'];
                return $pathReturn;
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }else{
            //default
            $defaultImage=ConfigGlobal::$rootWeb."/image/"."superai_group.png";
            return $defaultImage;
        }
    }
}