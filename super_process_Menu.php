<?php
include_once("Collection.php");
class Super_Process_Menu extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
	function getListMenu(){
		$sql="select * from category where parent=0";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getChildrenMenu($parent_id){
		$sql="select * from category where parent=$parent_id";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getListMusicInstrament(){
		$sql="select * from tag";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}

}