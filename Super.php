<?php
/*
This class is super core on this project never remove it
*/
@session_start();
class Super_Core{
	public static $url_data="http://woim.me/";
	public static $url_web="http://www.dev.woim.me/";
	public function Super_Core()
	{

	}

	public static function getModel($tablename,$primarykey,$nameUpperCase)
	{
		$nameClass = "super_process_" . $tablename . ".php";
		include_once("$nameClass");
		$upperCase = "Super_Process_" . $nameUpperCase;
		$objectReflection = new ReflectionClass($upperCase);
		$args = array($tablename, $primarykey);
		$object = $objectReflection->newInstanceArgs($args);
		return $object;
	}
	function toAscii($str) {
		//$clean = preg_replace("/[^a-zA-Z0-9/_|+ -]/",'', $str);
		//$clean = strtolower(trim($str, '-'));
		//$clean = preg_replace("/[/_|+ -]+/", '-', $clean);
		$clean=str_replace(" ","-",$str);
		return $clean;
	}
	public  static function getSenderEmail(){
		return "leminhtamboy@gmail.com";
	}
	public  static function Detected_Search($textValue,$textInput){
		return str_replace($textValue,"<b class='detect-text'>$textValue</b>",$textInput);
	}
	public static function getHeaderEmail(){
		return Super_Core::getRootWeb()."/Email/header.php";
	}
	public static function getFooterEmail(){
		return Super_Core::getRootWeb()."/Email/footer.html";
	}
	public function subStringLimit($limit,$string){
		if(strlen($string) > $limit) {
			return mb_substr($string, 0, $limit-6, "utf-8") . "...";
		}
		return $string;
	}
	public function subCommaInString($string){
		$string=substr($string,0,strlen($string)-1);
		return $string;
	}
	public function checkIssetParam($name,$method){
		$isset=false;
		switch ($method) {
			case 'post':
				if(isset($_POST[$name])){
					$isset=true;
				}
				break;
			case 'get':
				if(isset($_GET[$name])){
					$isset=true;
				}
				break;
			case 'request':
				if(isset($_REQUEST[$name])){
					$isset=true;
				}
				break;
			default:
				break;
		}
		return $isset;
	}
	public function getPost($name){
		return $_POST["$name"];
	}
	//unsecurity method for controller
	public function getGet($name){
		return $_GET["$name"];
	}
	public function getRequest($name){
		return $_REQUEST["$name"];
	}
	public function setSession($value,$key_session){
		$_SESSION[$key_session]=$value;
	}
	public function getSession($key_session){
		if($this->checkIssetSession($key_session)){
			return $_SESSION[$key_session];	
		}
		else{
			return null;
		}
	}
	public function checkIssetSession($key_session){
		if(isset($_SESSION[$key_session])){
			return true;
		}
		return false;
	}
	public function destroySession(){
		session_unset();
	}
	public function getRootWeb(){
		return self::$url_data;
	}
	public function getImgesPath(){
		return ConfigGlobal::$realPath."/images";
	}
	public function getImgesUrl(){
		return ConfigGlobal::$realPath."/images";	
	}
	public  function getDir(){
		return ConfigGlobal::$rootWeb;
	}
	public function getWebUrl(){
		return ConfigGlobal::$realPath;
	}
	public function getMediaUrl(){
		return  self::$url_data."/data/";
	}
	public static function getMediaUrlStatic(){
		return  self::$url_data."/data/";
	}
	public function getAjaxUrl(){
		return  $this->getWebUrl()."/Ajax/";
	}
	public function set_SESSIONCOVER($value){
		//$_SESSION["cover"]=$value;
	}
	public function get_SESSIONCOVER(){
		//return $_SESSION["cover"];
	}
	public function getCurrentUrl(){
		return  'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
	}
	public function processStringForInQuery($stringInput){
		$lastIndex=strrpos($stringInput,",");
		$stringInput=substr($stringInput,0,$lastIndex);
		return $stringInput;
	}
	//Image
	public function checkIssetImage($url){
		$file = $url; // 'images/'.$file (physical path)
		if(count(explode("http",$file)) > 1){
			$ch = curl_init($file);
			curl_setopt($ch, CURLOPT_NOBODY, true);
			curl_exec($ch);
			$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			if($code==200){
				return true;
			}else{
				return false;
			}
		}
		else{
			if (file_exists($file)) {
				return true;
			} else {
				return false;
			}
		}
	}
	public static function isMobile(){
		$userAgent=$_SERVER['HTTP_USER_AGENT'];
		$isIphone=preg_match('/iPad|iPhone|android|Android/',$userAgent);
		if($isIphone){
			return true;
		}else{
			return false;
		}
	}
	public function getArtistBanner(){
		echo $this->getImgesUrl()."/"."artist-default.jpg";
	}
	public function getMusicBanner(){
		return $this->getImgesUrl()."/"."music-banner-resized.gif";
	}
	public function getFolderImage(){
		return $this->getImgesUrl()."/"."folder.png";
	}

}
?>