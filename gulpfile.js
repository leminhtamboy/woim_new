'use strict';

var gulp = require('gulp'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require('browser-sync').create(),
    jshint = require('gulp-jshint');

gulp.task('scripts', function() {
    return gulp.src([
        'src/js/*.js',
      ])
      .pipe(jshint())
      .pipe(jshint.reporter('default'))
      .pipe(concat('custom.js'))
      .pipe(gulp.dest('script'))
      .pipe(rename({suffix: '.min'}))
      .pipe(uglify())
      .pipe(gulp.dest('script'))
      .pipe(browserSync.stream());
});

// TODO: Maybe we can simplify how sass compile the minify and unminify version
var compileSASS = function (filename, options) {
  return sass('src/scss/*.scss', options)
        .pipe(autoprefixer({
          browsers: ['last 10 versions'],
          cascade: false
        }))
        .pipe(concat(filename))
        .pipe(gulp.dest('css'))
        .pipe(browserSync.stream());
};

gulp.task('sass', function() {
    return compileSASS('custom.css', {});
});

gulp.task('sass-minify', function() {
    return compileSASS('custom.min.css', {style: 'compressed'});
});

gulp.task('browser-sync', function() {
    browserSync.init({
        proxy: "www.dev.woim.me"
    });
});

gulp.task('watch', function() {
  // Watch .js files
  gulp.watch('src/js/*.js', ['scripts']);
  // Watch .scss files
  gulp.watch(['src/scss/*.scss', 'src/scss/**/*.scss'], ['sass', 'sass-minify']);
});

// Default Task
gulp.task('default', ['browser-sync', 'watch']);