<?php
include_once("Collection.php");
class Super_Process_Artist extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
    function getDetailArtist($id_artist){
        $sql="select * from artist where id=$id_artist";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }

    function getListMenu(){
        $sql="select * from category where parent=0";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getChildrenMenu($parent_id){
        $sql="select * from category where parent=$parent_id";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getDetailCategory($id_category){
        $sql="select * from category where id=$id_category";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getAlbumInCategory($id){
        $sql="select * from album where category_id=$id order by id desc limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getSongInCategory($id_category){
        $sql="select * from song where category_id=$id_category order by id desc limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getArtistBySongId($artist_id){
        $sql="select * from artist where id=$artist_id";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getAlbumOfSong($album_id){
        $sql="select name,id from album where id=$album_id";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getAlbumByArtist($artist_id){
        $sql="select * from album where artist_id=$artist_id limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getSongByArtist($artist_id){
        $sql="select * from song where artist_id=$artist_id limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getCommentOnSong($song_id){
        $sql="select content,username from song_comment where song_id=$song_id limit 0,10";
        $data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getSuggestedSong(){
        //TODO
    }
}
