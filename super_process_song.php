<?php
include_once("Collection.php");
class Super_Process_Song extends Collection{

    function __construct($tableName,$primaryKey){
        parent::__construct($tableName,$primaryKey);
    }
    function getDetailSong($id_song){
    	$sql="select * from song where id=$id_song";
    	$data=$this->getCollectionBySql($sql);
        return $data;
    }
    function getLimitListenSong($id_user){
		$sql="select * from listen_album where user_id='$id_user'";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getListMenu(){
		$sql="select * from category where parent=0";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getChildrenMenu($parent_id){
		$sql="select * from category where parent=$parent_id";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function getDetailCategory($id_category){
		$sql="select * from category where id=$id_category";
		$data=$this->getCollectionBySql($sql);
        return $data;
	}
	function updateListen($id_song,$viewCount){
		$viewCount=$viewCount+1;
		$this->setData("id",$id_song);
		$this->setData("view",$viewCount);
		$this->updateRow();
	}
	function getAlbumInCategory($id){
		$sql="select * from album where category_id=$id order by id desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSongInCategory($id_category){
		$sql="select * from song where category_id=$id_category order by id desc limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getArtistBySongId($artist_id){
		$sql="select * from artist where id=$artist_id";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getAlbumOfSong($album_id){
		$sql="select name,id,name_ascii,cover,info,info_2 from album where id=$album_id";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getAlbumByArtist($artist_id){
		$sql="select * from album where artist_id=$artist_id limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSongByArtist($artist_id){
		$sql="select * from song where artist_id=$artist_id limit 0,10";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getCommentOnSong($song_id){
		$sql="select content,username from song_comment where song_id=$song_id  order by id desc limit 0,15";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function getSuggestedSong(){
		//TODO
	}
	function getLikeSong($id_album){
		$sql="select id from like_song where song_id=$id_album";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
	function checkIsLikeSong($id_album,$user_id){
		$sql="select id from like_song where song_id=$id_album and user_id='$user_id'";
		$data=$this->getCollectionBySql($sql);
		return $data;
	}
}